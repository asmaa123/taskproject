<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Absent
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $center_id
 * @property int|null $course_id
 * @property string|null $date
 * @property int|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Center|null $center
 * @property-read \App\Course|null $course
 * @property-read \App\Student|null $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Absent whereUpdatedAt($value)
 */
	class Absent extends \Eloquent {}
}

namespace App{
/**
 * App\Attend
 *
 * @property int $id
 * @property int $teacher_id
 * @property int|null $student_id
 * @property int|null $center_id
 * @property string|null $date
 * @property string|null $days
 * @property string|null $month
 * @property string|null $year
 * @property string|null $attends
 * @property string|null $leaves
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $teachers
 * @property-read int|null $teachers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereAttends($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereLeaves($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attend whereYear($value)
 */
	class Attend extends \Eloquent {}
}

namespace App{
/**
 * App\Center
 *
 * @property int $id
 * @property string|null $center_name
 * @property int|null $town_id
 * @property string|null $address
 * @property string|null $manager
 * @property string|null $head_manager
 * @property string|null $phone2
 * @property string|null $phone3
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $website
 * @property int|null $active
 * @property int|null $age
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $type_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Absent[] $Absents
 * @property-read int|null $absents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attend[] $Attends
 * @property-read int|null $attends_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseType[] $coursetypes
 * @property-read int|null $coursetypes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $teachers
 * @property-read int|null $teachers_count
 * @property-read \App\Town|null $town
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereCenterName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereHeadManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center wherePhone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center wherePhone3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereTownId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Center whereWebsite($value)
 */
	class Center extends \Eloquent {}
}

namespace App{
/**
 * App\CenterDocument
 *
 * @property int $id
 * @property int|null $center_id
 * @property string|null $file
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CenterDocument whereUpdatedAt($value)
 */
	class CenterDocument extends \Eloquent {}
}

namespace App{
/**
 * App\Contact
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ContactTrans[] $details
 * @property-read int|null $details_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact query()
 */
	class Contact extends \Eloquent {}
}

namespace App{
/**
 * App\ContactTrans
 *
 * @property-read \App\Contact $contact
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactTrans newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactTrans newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactTrans query()
 */
	class ContactTrans extends \Eloquent {}
}

namespace App{
/**
 * App\Count
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $amount
 * @property int|null $discount
 * @property string|null $date
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Student|null $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Count whereUpdatedAt($value)
 */
	class Count extends \Eloquent {}
}

namespace App{
/**
 * App\Course
 *
 * @property int $id
 * @property string|null $course_name
 * @property int|null $max_num
 * @property int|null $time
 * @property int|null $center_id
 * @property int|null $coursetype_id
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $type_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Absent[] $Absents
 * @property-read int|null $absents_count
 * @property-read \App\CourseType|null $CourseType
 * @property-read \App\Center|null $center
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseMaterial[] $details
 * @property-read int|null $details_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseLevel[] $levels
 * @property-read int|null $levels_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Material[] $materials
 * @property-read int|null $materials_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $teachers
 * @property-read int|null $teachers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseTime[] $times
 * @property-read int|null $times_count
 * @property-read \App\Type|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCourseName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCoursetypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereMaxNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 */
	class Course extends \Eloquent {}
}

namespace App{
/**
 * App\CourseLevel
 *
 * @property int $id
 * @property int|null $course_id
 * @property int $coursetype_id
 * @property int|null $level_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Course $course
 * @property-read \App\Level|null $level
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel whereCoursetypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLevel whereUpdatedAt($value)
 */
	class CourseLevel extends \Eloquent {}
}

namespace App{
/**
 * App\CourseMaterial
 *
 * @property int $id
 * @property int|null $course_id
 * @property int|null $material_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Course|null $course
 * @property-read \App\Material|null $material
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseMaterial whereUpdatedAt($value)
 */
	class CourseMaterial extends \Eloquent {}
}

namespace App{
/**
 * App\CourseTime
 *
 * @property int $id
 * @property int|null $course_id
 * @property string|null $day
 * @property string|null $time_from
 * @property string|null $time_to
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Course|null $course
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime whereTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime whereTimeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTime whereUpdatedAt($value)
 */
	class CourseTime extends \Eloquent {}
}

namespace App{
/**
 * App\CourseType
 *
 * @property int $id
 * @property string|null $type_name
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $Courses
 * @property-read int|null $courses_count
 * @property-read \App\Center $center
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Level[] $levels
 * @property-read int|null $levels_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Season[] $seasons
 * @property-read int|null $seasons_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $teachers
 * @property-read int|null $teachers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType whereTypeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseType whereUpdatedAt($value)
 */
	class CourseType extends \Eloquent {}
}

namespace App{
/**
 * App\Doc
 *
 * @property int $id
 * @property string|null $file
 * @property int|null $type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @property-read \App\Type|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Doc whereUpdatedAt($value)
 */
	class Doc extends \Eloquent {}
}

namespace App{
/**
 * App\Exemption
 *
 * @property int $id
 * @property string $ratio
 * @property string $reason
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exemption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exemption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exemption query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exemption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exemption whereRatio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exemption whereReason($value)
 */
	class Exemption extends \Eloquent {}
}

namespace App{
/**
 * App\Guardian
 *
 * @property int $id
 * @property string|null $image
 * @property string|null $guardian_name
 * @property string|null $username
 * @property string|null $password
 * @property string|null $recover
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $phone2
 * @property string|null $job
 * @property string|null $whatsapp
 * @property string|null $national_id
 * @property string|null $address
 * @property string|null $nationality
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereGuardianName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereNationalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian wherePhone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereRecover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Guardian whereWhatsapp($value)
 */
	class Guardian extends \Eloquent {}
}

namespace App{
/**
 * App\Job
 *
 * @property int $id
 * @property string|null $job_name
 * @property string|null $job_type
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $teachers
 * @property-read int|null $teachers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereJobName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereJobType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Job whereUpdatedAt($value)
 */
	class Job extends \Eloquent {}
}

namespace App{
/**
 * App\Level
 *
 * @property int $id
 * @property string|null $level_name
 * @property int|null $course_id
 * @property int $coursetype_id
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\CourseType $coursetype
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Material[] $materials
 * @property-read int|null $materials_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level whereCoursetypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level whereLevelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Level whereUpdatedAt($value)
 */
	class Level extends \Eloquent {}
}

namespace App{
/**
 * App\LevelMaterial
 *
 * @property int $id
 * @property int|null $level_id
 * @property int|null $material_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Level|null $level
 * @property-read \App\Material|null $material
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LevelMaterial whereUpdatedAt($value)
 */
	class LevelMaterial extends \Eloquent {}
}

namespace App{
/**
 * App\Material
 *
 * @property int $id
 * @property string|null $material_name
 * @property int|null $success
 * @property int|null $p1
 * @property int|null $p2
 * @property int|null $p3
 * @property int|null $p4
 * @property int|null $p5
 * @property int|null $level_id
 * @property int|null $course_id
 * @property int|null $active
 * @property \Illuminate\Database\Eloquent\Collection|\App\CourseMaterial[] $details
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Part[] $Parts
 * @property-read int|null $parts_count
 * @property-read int|null $details_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeacherMaterial[] $teachers
 * @property-read int|null $teachers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereMaterialName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereP1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereP2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereP3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereP4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereP5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereSuccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Material whereUpdatedAt($value)
 */
	class Material extends \Eloquent {}
}

namespace App{
/**
 * App\MaterialDocument
 *
 * @property int $id
 * @property int|null $material_id
 * @property string|null $file
 * @property string $details
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MaterialDocument whereUpdatedAt($value)
 */
	class MaterialDocument extends \Eloquent {}
}

namespace App{
/**
 * App\Member
 *
 * @property int $id
 * @property string|null $image
 * @property string|null $guardian_name
 * @property int|null $guardian_id
 * @property string|null $username
 * @property string|null $password
 * @property string|null $recover
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $job
 * @property string|null $whatsapp
 * @property string|null $national_id
 * @property string|null $address
 * @property string|null $nationality
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereGuardianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereGuardianName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereNationalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereRecover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereWhatsapp($value)
 */
	class Member extends \Eloquent {}
}

namespace App{
/**
 * App\Message
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message query()
 */
	class Message extends \Eloquent {}
}

namespace App{
/**
 * App\Org
 *
 * @property int $id
 * @property string|null $logo
 * @property string|null $name
 * @property string|null $address
 * @property string|null $business_registration
 * @property string|null $tax_card
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $email
 * @property string|null $website
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereBusinessRegistration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereTaxCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Org whereWebsite($value)
 */
	class Org extends \Eloquent {}
}

namespace App{
/**
 * App\Page
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $page_name
 * @property string $page_route
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page wherePageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page wherePageRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page whereUpdatedAt($value)
 */
	class Page extends \Eloquent {}
}

namespace App{
/**
 * App\Page_permission
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page_permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page_permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Page_permission query()
 */
	class Page_permission extends \Eloquent {}
}

namespace App{
/**
 * App\Part
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property int|null $material_id
 * @property string|null $final
 * @property string|null $total
 * @property string|null $percent
 * @property string|null $month
 * @property string|null $year
 * @property int|null $day
 * @property int|null $part
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Material|null $Material
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @property-read \App\Type $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereFinal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part wherePart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Part whereYear($value)
 */
	class Part extends \Eloquent {}
}

namespace App{
/**
 * App\Paym
 *
 * @property int $id
 * @property int|null $student_id
 * @property int $amount
 * @property int $remain
 * @property int|null $month
 * @property int|null $year
 * @property int $payed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym wherePayed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereRemain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paym whereYear($value)
 */
	class Paym extends \Eloquent {}
}

namespace App{
/**
 * App\Payment
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $amount
 * @property int|null $remain
 * @property string|null $notes
 * @property string|null $code
 * @property string|null $type
 * @property string|null $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereRemain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUpdatedAt($value)
 */
	class Payment extends \Eloquent {}
}

namespace App{
/**
 * App\Percent
 *
 * @property int $id
 * @property string|null $percent_name
 * @property int|null $grade
 * @property int|null $material_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent whereGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent wherePercentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Percent whereUpdatedAt($value)
 */
	class Percent extends \Eloquent {}
}

namespace App{
/**
 * App\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $route
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissons
 * @property-read int|null $permissons_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $perms
 * @property-read int|null $perms_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\Salary
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property int|null $salary
 * @property int|null $days
 * @property string|null $hours
 * @property string|null $month
 * @property string|null $year
 * @property int|null $bonus
 * @property int|null $minus
 * @property int|null $parts
 * @property int|null $final
 * @property int|null $status
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereFinal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereMinus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereParts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Salary whereYear($value)
 */
	class Salary extends \Eloquent {}
}

namespace App{
/**
 * App\Season
 *
 * @property int $id
 * @property string|null $season_name
 * @property string|null $season_year
 * @property int|null $price
 * @property int $coursetype_id
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\CourseType $CourseType
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereCoursetypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereSeasonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereSeasonYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereUpdatedAt($value)
 */
	class Season extends \Eloquent {}
}

namespace App{
/**
 * App\Student
 *
 * @property int $id
 * @property string|null $image
 * @property string|null $student_name
 * @property string|null $birth
 * @property string|null $gender
 * @property string|null $address
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $national_id
 * @property int|null $exemption_id
 * @property int|null $material_id
 * @property string|null $first_day
 * @property string|null $year
 * @property int|null $age
 * @property int|null $guardian_id
 * @property int|null $coursetype_id
 * @property int|null $studentgrade_id
 * @property string|null $nationality
 * @property int|null $transportation
 * @property int|null $center_id
 * @property int|null $level_id
 * @property int|null $part_id
 * @property int|null $attend_id
 * @property int|null $course_id
 * @property int|null $season_id
 * @property int|null $active
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Absent[] $absent
 * @property-read int|null $absent_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\StudentArchive[] $archives
 * @property-read int|null $archives_count
 * @property-read \App\Attend|null $attend
 * @property-read \App\Center|null $center
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Count[] $counts
 * @property-read int|null $counts_count
 * @property-read \App\Course|null $course
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\StudentCourse[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\StudentMaterial[] $details
 * @property-read int|null $details_count
 * @property-read \App\Exemption|null $exemption
 * @property-read \App\Guardian|null $guardian
 * @property-read \App\Level|null $level
 * @property-read \App\Material|null $material
 * @property-read \App\Part|null $part
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Payment[] $payments
 * @property-read int|null $payments_count
 * @property-read \App\Season|null $season
 * @property-read \App\StudentGrade|null $studentGrade
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereAttendId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereCoursetypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereExemptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereFirstDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereGuardianId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereNationalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student wherePartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereSeasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereStudentName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereStudentgradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereTransportation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereYear($value)
 */
	class Student extends \Eloquent {}
}

namespace App{
/**
 * App\StudentArchive
 *
 * @property int $id
 * @property int|null $center_id
 * @property int|null $level_id
 * @property int|null $season_id
 * @property int|null $student_id
 * @property string|null $year
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Center|null $center
 * @property-read \App\Level|null $level
 * @property-read \App\Season|null $season
 * @property-read \App\Student|null $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereSeasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentArchive whereYear($value)
 */
	class StudentArchive extends \Eloquent {}
}

namespace App{
/**
 * App\StudentCourse
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $course_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Course|null $course
 * @property-read \App\Student|null $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentCourse whereUpdatedAt($value)
 */
	class StudentCourse extends \Eloquent {}
}

namespace App{
/**
 * App\StudentDocument
 *
 * @property int $id
 * @property int|null $student_id
 * @property string|null $file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentDocument whereUpdatedAt($value)
 */
	class StudentDocument extends \Eloquent {}
}

namespace App{
/**
 * App\StudentGrade
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $material_id
 * @property int|null $center_id
 * @property string|null $date
 * @property string|null $save
 * @property string|null $tajweed
 * @property string|null $performance
 * @property int|null $total
 * @property string|null $percent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\StudentPercent[] $details
 * @property-read int|null $details_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @property-read int|null $students_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade wherePerformance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereSave($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereTajweed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentGrade whereUpdatedAt($value)
 */
	class StudentGrade extends \Eloquent {}
}

namespace App{
/**
 * App\StudentLevel
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $level_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Level|null $level
 * @property-read \App\Student|null $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentLevel whereUpdatedAt($value)
 */
	class StudentLevel extends \Eloquent {}
}

namespace App{
/**
 * App\StudentMaterial
 *
 * @property int $id
 * @property int|null $student_id
 * @property int|null $material_id
 * @property int|null $status
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentMaterial whereUpdatedAt($value)
 */
	class StudentMaterial extends \Eloquent {}
}

namespace App{
/**
 * App\StudentPercent
 *
 * @property int $id
 * @property int|null $grade_id
 * @property int|null $material_id
 * @property int|null $percent_id
 * @property \App\StudentGrade|null $grade
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent whereGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent whereGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent wherePercentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StudentPercent whereUpdatedAt($value)
 */
	class StudentPercent extends \Eloquent {}
}

namespace App{
/**
 * App\Subscriber
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscriber newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscriber newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscriber query()
 */
	class Subscriber extends \Eloquent {}
}

namespace App{
/**
 * App\Teacher
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $teacher_name
 * @property string|null $username
 * @property string|null $password
 * @property string|null $recover
 * @property string|null $birth
 * @property string|null $address
 * @property string|null $email
 * @property \App\Job|null $job
 * @property string|null $phone
 * @property int|null $job_id
 * @property int|null $hours
 * @property string|null $image
 * @property int|null $salary
 * @property string|null $first_day
 * @property string|null $holiday
 * @property string|null $national_id
 * @property int|null $center_id
 * @property int|null $course_id
 * @property int|null $coursetype_id
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Center|null $center
 * @property-read \App\CourseType|null $coursetype
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeacherMaterial[] $details
 * @property-read int|null $details_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeacherLevel[] $levels
 * @property-read int|null $levels_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereCoursetypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereFirstDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereHoliday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereJobId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereNationalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereRecover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereTeacherName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereUsername($value)
 */
	class Teacher extends \Eloquent {}
}

namespace App{
/**
 * App\TeacherCourse
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property int|null $course_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Course|null $course
 * @property-read \App\Teacher|null $teacher
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherCourse whereUpdatedAt($value)
 */
	class TeacherCourse extends \Eloquent {}
}

namespace App{
/**
 * App\TeacherDocument
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property string|null $file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherDocument whereUpdatedAt($value)
 */
	class TeacherDocument extends \Eloquent {}
}

namespace App{
/**
 * App\TeacherLevel
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property int|null $level_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Level|null $level
 * @property-read \App\Teacher|null $teacher
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherLevel whereUpdatedAt($value)
 */
	class TeacherLevel extends \Eloquent {}
}

namespace App{
/**
 * App\TeacherMaterial
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property int|null $material_id
 * @property int|null $status
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Material|null $material
 * @property-read \App\Teacher|null $teacher
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial whereMaterialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeacherMaterial whereUpdatedAt($value)
 */
	class TeacherMaterial extends \Eloquent {}
}

namespace App{
/**
 * App\Time
 *
 * @property int $id
 * @property int|null $teacher_id
 * @property string|null $attend
 * @property string|null $leave
 * @property string|null $day
 * @property string|null $day_ar
 * @property int|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereAttend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereDayAr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereLeave($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Time whereUpdatedAt($value)
 */
	class Time extends \Eloquent {}
}

namespace App{
/**
 * App\Town
 *
 * @property int $id
 * @property string|null $town_name
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Center[] $centers
 * @property-read int|null $centers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereTownName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Town whereUpdatedAt($value)
 */
	class Town extends \Eloquent {}
}

namespace App{
/**
 * App\Transportation
 *
 * @property int $id
 * @property int|null $sat
 * @property int|null $sun
 * @property int|null $mon
 * @property int|null $tue
 * @property int|null $wed
 * @property int|null $thu
 * @property int|null $fri
 * @property string|null $arrival
 * @property string|null $launch
 * @property int|null $price
 * @property string|null $bus
 * @property int|null $manager_id
 * @property int|null $center_id
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereArrival($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereBus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereFri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereLaunch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereMon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereSat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereSun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereThu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereTue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transportation whereWed($value)
 */
	class Transportation extends \Eloquent {}
}

namespace App{
/**
 * App\Type
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $center_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Doc[] $doc
 * @property-read int|null $doc_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Level[] $levels
 * @property-read int|null $levels_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Part[] $parts
 * @property-read int|null $parts_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereCenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Type whereUpdatedAt($value)
 */
	class Type extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $name_en
 * @property string|null $username
 * @property string|null $type
 * @property string|null $mobile
 * @property string|null $website
 * @property string|null $about
 * @property string|null $email
 * @property string|null $password
 * @property string|null $recover
 * @property string|null $country
 * @property string|null $facebook
 * @property string|null $twitter
 * @property string|null $google
 * @property string|null $instagram
 * @property string|null $linkedin
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $details
 * @property string|null $image
 * @property string|null $remember_token
 * @property int|null $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $rolelist
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGoogle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLinkedin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRecover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWebsite($value)
 */
	class User extends \Eloquent {}
}

