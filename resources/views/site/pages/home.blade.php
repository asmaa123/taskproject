@extends('site.layouts.master')
@section('title')
حفاظ
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
    <section class="content">
      @if(Auth::guard('members')->check())
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="box cases collapsed-box">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-edit"></i> دفع مديونيات الطلاب</h3>
              <div class="box-tools pull-right">
                <a class="btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></a>
                <a class="btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                    <tr>
                      <th class="num">#</th>
                      <th>الطالب</th>
                      <th>المركز</th>
                      <th>الفصل الدراسي</th>
                      <th>الحساب</th>
                      <th>الدفع</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($students as $student)
                    <tr>
                      <td class="num">{{$loop->index + 1}}</td>
                      <td>{{$student->student_name}}</td>
                      <td>{{$student->center_name}}</td>
                      <td>{{$student->season_name}}</td>
                      @if($student->amount >= 0)
                      <td>الرصيد : {{$student->amount}}</td>
                      @elseif($student->amount < 0)
                      <td>الديون : {{$student->amount * -1}}</td>
                      @endif
                      <td>
                      @php 
                      $StudentId = $student->id;
                      $TranAmount = $student->amount * -1;
                      $TranTrackid=mt_rand();
                      $TranportalId="104701";
                      $ReqTranportalId="id=".$TranportalId;
                      $ReqTranportalPassword="password=104701pg";
                      $ReqAmount="amt=".$TranAmount;
                      $ReqTrackId="trackid=".$TranTrackid;
                      $ReqCurrency="currencycode=414";
                      $ReqLangid="langid=AR";
                      $ReqAction="action=1";
                      $ResponseUrl="http://trial-hofaz.lubic-kw.com/GetHandlerResponse.php";
                      $ReqResponseUrl="responseURL=".$ResponseUrl;
                      $ErrorUrl=route('site.error');
                      $ReqErrorUrl="errorURL=".$ErrorUrl;
                      $ReqUdf1= "udf1=".$StudentId;
                      $ReqUdf2="udf2=Test2";
                      $ReqUdf3="udf3=Test3";
                      $ReqUdf4="udf4=Test4";
                      $ReqUdf5="udf5=Test5";
                      $param=$ReqTranportalId."&".$ReqTranportalPassword."&".$ReqAction."&".$ReqLangid."&".$ReqCurrency."&".$ReqAmount."&".$ReqResponseUrl."&".$ReqErrorUrl."&".$ReqTrackId."&".$ReqUdf1."&".$ReqUdf2."&".$ReqUdf3."&".$ReqUdf4."&".$ReqUdf5;
                      $termResourceKey="9069006100649069";
                      $blocksize = 16;
                      $pad = $blocksize - (strlen($param) % $blocksize);
                      $str = $param . str_repeat(chr($pad), $pad);
                      $encrypted = openssl_encrypt($str, 'AES-128-CBC', $termResourceKey, OPENSSL_ZERO_PADDING, $termResourceKey);
                      $encrypted = base64_decode($encrypted);
                      $encrypted=unpack('C*', ($encrypted));
                      $chars = array_map("chr", $encrypted);
                      $bin = join($chars);
                      $encrypted=bin2hex($bin);
                      $encrypted = urlencode($encrypted);
                      $param=$encrypted."&tranportalId=".$TranportalId."&responseURL=".$ResponseUrl."&errorURL=".$ErrorUrl;
                      $url = "https://www.kpaytest.com.kw/kpg/PaymentHTTP.htm?param=paymentInit"."&trandata=".$param;
                      @endphp
                      @if($student->amount < 0)
                      <a href="{{$url}}" class="btn btn-blue">ادفع</a>
                      @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> 
            </div>
          </div> 
        </div><!-- /.col -->
      </div><!-- /.row -->

        <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="box cases collapsed-box">
                    <div class="box-header">
                      <h3 class="box-title"><i class="fa fa-edit"></i> تقارير عمليات الدفع</h3>
                      <div class="box-tools pull-right">
                        <a class="btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></a>
                        <a class="btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                        <div class="box-body">
                        <div class="col-md-4">                   
                        <div class="form-group">
                                <h5>الطالب</h5>
                                <select class="form-control select2" id="student_pays" name="student_id">
                                <option>            </option>
                                    @foreach($students as $student)
                                        <option value="{{$student->id}}">{{$student->student_name}}</option>
                                    @endforeach
                                </select>
                        </div>
                        </div>

                        <div class="col-md-4">
                        <div class="form-group">
                            <h5>السنة</h5>
                            <select name="year" id="years" class="form-control select2" data-placeholder="اختر السنة" style="width: 100%; text-align: right;">
                                <option>               </option>
                                <option value="2019/2020">2019/2020</option>
                                            <option value="2020/2021">2020/2021</option>
                                            <option value="2021/2022">2021/2022</option>
                                            <option value="2022/2023">2022/2023</option>
                                            <option value="2023/2024">2023/2024</option>
                                            <option value="2024/2025">2024/2025</option>
                                            <option value="2025/2026">2025/2026</option>
                                            <option value="2026/2027">2026/2027</option>
                                            <option value="2027/2028">2027/2028</option>
                                            <option value="2028/2029">2028/2029</option>
                                            <option value="2029/2030">2029/2030</option>
                                            <option value="2030/2031">2030/2031</option>
                                            <option value="2031/2032">2031/2032</option>
                                            <option value="2032/2033">2032/2033</option>
                                            <option value="2033/2034">2033/2034</option>
                                            <option value="2034/2035">2034/2035</option>
                                            <option value="2035/2036">2035/2036</option>
                                            <option value="2036/2037">2036/2037</option>
                                            <option value="2037/2038">2037/2038</option>
                                            <option value="2038/2039">2038/2039</option>
                                            <option value="2039/2040">2039/2040</option>
                                            <option value="2040/2041">2040/2041</option>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                            <h5>الشهر</h5>
                            <select name="month" id="month" class="form-control select2" data-placeholder="اختر الشهر" style="width: 100%; text-align: right;">
                                <option>               </option>
                                <option value="1">يناير</option>
                                <option value="2">فبراير</option>
                                <option value="3">مارس</option>
                                <option value="4">ابريل</option>
                                <option value="5">مايو</option>
                                <option value="6">يونيو</option>
                                <option value="7">يوليو</option>
                                <option value="8">أغسطس</option>
                                <option value="9">سبتمبر</option>
                                <option value="10">أكتوبر</option>
                                <option value="11">نوفمبر</option>
                                <option value="12">ديسمبر</option>
                            </select>
                            <input name="date" type="hidden" class="form-control" style="text-align: right;" value="{{$now->toDateString()}}">
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                        <table class="table table-striped table-bordered text-center">
                                <tbody id="process">
                                    <tr><td>التاريخ</td><td>المبلغ</td></tr>
                                </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                  </div> 
                </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="box cases collapsed-box">
                    <div class="box-header">
                      <h3 class="box-title"><i class="fa fa-edit"></i> تقارير حضور الطلاب</h3>
                      <div class="box-tools pull-right">
                        <a class="btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></a>
                        <a class="btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                    <div class="box-body">
                      <div class="col-md-3">                 
                      <div class="form-group">
                            <h5>من</h5>
                            <input id="dfrom" type="date" class="form-control" style="text-align: right;" value="{{$now->toDateString()}}">
                      </div>
                      </div>
                      <div class="col-md-3"> 
                      <div class="form-group">
                            <h5>الى</h5>
                            <input id="dto" type="date" class="form-control" style="text-align: right;" value="{{$now->toDateString()}}">
                      </div>
                      </div>
                      <div class="col-md-12"> 
                        <table class="table table-striped table-bordered text-center" id="tables">
                            <tbody id="absents">
                                <tr></tr>
                            </tbody>
                      </table>
                    </div>
                    </div>
                  </div> 
                </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="box cases collapsed-box">
                    <div class="box-header">
                      <h3 class="box-title"><i class="fa fa-edit"></i> تقارير درجات الطلاب</h3>
                      <div class="box-tools pull-right">
                        <a class="btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></a>
                        <a class="btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-3">                  
                      <div class="form-group">
                      <h5>الطالب</h5>
                        <select class="form-control select2" id="students" name="student_id">
                        <option>            </option>
                            @foreach($students as $student)
                                <option value="{{$student->id}}">{{$student->student_name}}</option>
                            @endforeach
                        </select>
                      </div>
                      </div>
                        <div class="col-md-3"> 
                      <div class="form-group">
                      <h5>المادة</h5>
                        <select class="form-control select2" id="materials" name="material_id">
                            <option >              </option>
                        </select>
                      </div>
                      </div>
                        <div class="col-md-3"> 
                      <div class="form-group">
                            <h5>من</h5>
                            <input id="from" type="date" class="form-control" style="text-align: right;" value="{{$now->toDateString()}}">
                      </div>
                      </div>
                        <div class="col-md-3"> 
                      <div class="form-group">
                            <h5>الى</h5>
                            <input id="to" type="date" class="form-control" style="text-align: right;" value="{{$now->toDateString()}}">
                      </div>
                      </div>
                        <div class="col-md-12"> 
                        <table class="table table-striped table-bordered text-center" id="tables">
                            <thead><tr><th>المادة</th><th>الدرجة</th><th>التقدير</th><th>التاريخ</th><th>عرض</th></tr></thead>
                            <tbody id="per">
                                <tr>
                                    
                                </tr>
                            </tbody>
                      </table>
                    </div>
                    <div class="modal fade" id="modal-default">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">تفاصيل المادة</h4>
                        </div>
                        <div class="modal-body">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered text-center" id="percents">
                            <tbody>
                            <tr>
                                <th>البند</th>
                                <th>الدرجة</th>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-orange pull-left" data-dismiss="modal">اغلق</button>
                        </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                    </div>
                  </div> 
                </div><!-- /.col -->
        </div><!-- /.row -->
    @endif
    </section>
  </div>
  <!-- Content page End -->
@endsection