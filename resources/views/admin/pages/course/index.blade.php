@extends('admin.layouts.master')
@section('title')
الحلقات
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">الحلقات</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                        <table id="tables" class="display" style="width:100%">
                            <thead>
                            <tr>
                                <th class="num">#</th>
                                <th>الاسم</th>
                                 <th>نوع الحلقه</th>
                                <th>المركز</th>
                                <th>الحد الاقصى</th>
                                <th>العمليات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($courses as $course)
                            <tr>
                                <td class="num">{{$loop->index + 1}}</td>
                                <td>{{ isset($course->course_name) ? $course->course_name : ''}}</td>
                                <td>{{isset($course->CourseType->type_name) ? $course->CourseType->type_name :''}}</td>
                              <td>{{isset($course->center->center_name) ? $course->center->center_name   : ''}}</td>
                                <td>{{ isset($course->max_num) ? $course->max_num   :''}}</td>
                                <td class="action">
                                <!--<a href="{{ route('admin.courses.edit' , ['id' => $course->id]) }}" class="btn btn-green"> عرض   </a>-->
                                <a href="{{ route('admin.courses.edit' , ['id' => $course->id]) }}" title="تعديل"> <i class="fa fa-edit"></i>   </a>
                                <a class="btndelet" href="{{ route('admin.courses.delete' , ['id' => $course->id]) }}"  title="حذف">
                                <i class="fa fa-trash"></i>
                                </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
  <!-- Content page End -->
@endsection

