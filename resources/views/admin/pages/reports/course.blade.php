@extends('admin.layouts.master')
@section('title')
تقارير عدد حلقات الطلاب 
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
    <!--<section class="content-header">-->
    <!--  <h1>-->
    <!--    <i class="fa fa-arrow-left"></i>-->
    <!--    <span class="semi-bold">Dashboard</span>-->
    <!--    <small>Control panel</small>-->
    <!--  </h1>-->
    <!--  <ol class="breadcrumb">-->
    <!--    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>-->
    <!--    <li class="active">Dashboard</li>-->
    <!--  </ol>-->
    <!--</section>-->-->
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تقارير الحلقات</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
				
                   
                    <div class="box-body">
                    <table id="tables" class="display dataTable no-footer dtr-inline" style="width:100%">
                        <thead>
                           <tr>
                            <th class="num">#</th>
                         <th>اسم الطالب</th>
                            <th>المركز</th>
                             <th>الحلقه</th>
                              <th>المستوي</th>
                              <th>  الفصل الدراسي</th>
                              <th>العمليات</th>

                         <tr class="tr-head">
                                <th>الترتيب</th>
                           <th>اسم الطالب</th>
                            <th>المركز</th>
                             <th>الحلقه</th>
                              <th>المستوي</th>
                              <th>  الفصل الدراسي</th>   
                                <th>العمليات</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($students as $student)
                 
                     
                        <tr>
                       <td class="num">{{ $loop->iteration }}</td>
                        <td>{{$student->student_name}}     {{$student->national_id}}</td>
                           <td>{{ isset($student->center->center_name)  ? $student->center->center_name : ''}} </td>
                           
                              <td>{{ isset($student->course->course_name) ?  $student->course->course_name  : ''}} </td>
                              
                        <td>{{ isset($student->level->level_name) ?  $student->level->level_name  : ''}} </td>
                       
                    
                      
                     
                        <td>{{$student->season_name}}</td>
                        
                         <td>  <a href="{{ route('admin.reports.print' , ['id' => $student->id]) }}" title="طباعه"> <i class="fa fa-print""></i>  طباعه  </a></td>

                       
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
  </div>
  <!-- Content page End -->
@endsection