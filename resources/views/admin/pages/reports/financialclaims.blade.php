@extends('admin.layouts.master')
@section('title')
تقارير حضور الطلاب
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold"> تقارير حضور الطلاب</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                       <table id="tables" class="display dataTable no-footer dtr-inline" style="width:100%">
                        <thead>
                        <tr>
                            <th class="num">#</th>
                            <th>المدفوع</th>
                            <th>نوع الاعضاء</th>
                             <th>الباقي</th>
                              <th>الجنس</th>
                               <th>هاتف الاب</th>
                                <th>هاتف الام</th>
                           
                           
                        </tr>
                           <tr class="tr-head">
                                <th>الترتيب</th>
                              <th>المدفوع</th>
                            <th>نوع الاعضاء</th>
                             <th>الباقي</th>
                              <th>الجنس</th>
                               <th>هاتف الاب</th>
                                <th>هاتف الام</th
                                 </tr>
                        </thead>
                        <tbody>
                        @foreach( $students as  $student)
                      
                      
                        <tr>
                       <td class="num">{{ $loop->iteration }}</td>
                        <td>{{isset($student->payments->first()->amount) ?  $student->payments->first()->amount :''}}     {{$student->national_id}}</td>
                    
                      <td>{{$student->gender}}</td>
                       
                    <td>{{isset($student->payments->first()->remain) ?  $student->payments->first()->remain :''}}    </td>
                     <td>{{isset($student->guardian->nationality) ?  $student->guardian->nationality:''}}    </td>
                      <td>{{isset($student->guardian->phone) ?  $student->guardian->phone:''}}    </td>
                       <td>{{isset($student->guardian->phone2) ?  $student->guardian->phone2:''}}    </td>
                       
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>



                </div>
            </div>
        </div>
    </section>


</div>
  <!-- Content page End -->
@endsection

