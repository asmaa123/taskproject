@extends('admin.layouts.master') 
@section('title')
المواصلات 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات المواصلات</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.transportations')}}"> بيانات المواصلات</a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تعديل بيانات المواصلات</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
          			@foreach($transportations as $transportation)
						<form class="form-1" action="{{ route('admin.transportations.edit' , ['id' => $transportation->id]) }}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row form-row">
								
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">محطة الوصول</label>
										<input name="arrival" value="{{$transportation->arrival}}" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">محطة الانطلاق</label>
										<input name="launch" value="{{$transportation->launch}}" class="form-control" type="text">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>المركز</label>
										<select name="center" class="form-control pmd-select2 select2">
											<option value="{{$transportation->center_id}}" selected>{{$transportation->center_name}}</option>
										@foreach($centers as $center)
											<option value="{{$center->id}}">{{$center->center_name}}</option>
										@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>المشرف</label>
										<select name="manager" class="form-control pmd-select2 select2">
											<option value="{{$transportation->manager_id}}" selected>{{$transportation->teacher_name}}</option>
											@foreach($teachers as $teacher)
											<option value="{{$teacher->id}}">{{$teacher->teacher_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
                				<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">رقم الاوتوبيس</label>
										<input name="bus" value="{{$transportation->bus}}" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">قيمة الاشتراك</label>
										<input name="price" value="{{$transportation->price}}" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" @if($transportation->active == 1) checked @endif>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<h4>الأيام</h4>
										<label class="checkbox-inline">السبت</label>
										<input class="minimal" name="sat" type="checkbox" @if($transportation->sat == 1) checked @endif>
										<label class="checkbox-inline">الأحد</label>
										<input class="minimal" name="sun" type="checkbox" @if($transportation->sun == 1) checked @endif>
										<label class="checkbox-inline">الاثنين</label>
										<input class="minimal" name="mon" type="checkbox" @if($transportation->mon == 1) checked @endif>
										<label class="checkbox-inline">الثلاثاء</label>
										<input class="minimal" name="tue" type="checkbox" @if($transportation->tue == 1) checked @endif>
										<label class="checkbox-inline">الأربعاء</label>
										<input class="minimal" name="wed" type="checkbox" @if($transportation->wed == 1) checked @endif>
										<label class="checkbox-inline">الخميس</label>
										<input class="minimal" name="thu" type="checkbox" @if($transportation->thu == 1) checked @endif>
										<label class="checkbox-inline">الجمعة</label>
										<input class="minimal" name="fri" type="checkbox" @if($transportation->fri == 1) checked @endif>
									</div>
								</div>
								<div class="col-md-12">
									<br> <a href="{{route('admin.transportations')}}" class="btn btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
            		@endforeach
					</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection