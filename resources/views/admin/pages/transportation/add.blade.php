@extends('admin.layouts.master') 
@section('title')
المواصلات 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات المواصلات</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.transportations')}}"> بيانات المواصلات</a></li>
        <li class="active">اضافة</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">اضافة مواصلة جديد</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
						<form class="mtb-15" action="{{route('admin.transportations.add')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row form-row">
								
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">محطة الوصول</label>
										<input name="arrival" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">محطة الانطلاق</label>
										<input name="launch" class="form-control" type="text">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>المركز</label>
										<select name="center"  class="form-control pmd-select2 select2">
											<option></option>
											@foreach($centers as $center)
											<option value="{{$center->id}}">{{$center->center_name}}</option>@endforeach</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>المشرف</label>
										<select name="manager"  class="form-control pmd-select2 select2">
											<option></option>
                                            @foreach($teachers as $teacher)
											<option value="{{$teacher->id}}">{{$teacher->teacher_name}}</option>
                                            @endforeach
                                        </select>
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">رقم الاوتوبيس</label>
										<input name="bus" class="form-control" type="text">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">قيمة الاشتراك</label>
										<input name="price" class="form-control" type="text">
									</div>
								</div>
                                <div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" checked>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
								</div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h4>الأيام</h4>
                                        <label class="checkbox-inline">السبت</label>
                                        <input class="minimal" name="sat" type="checkbox" >
                                        <label class="checkbox-inline">الأحد</label>
                                        <input class="minimal" name="sun" type="checkbox" >
                                        <label class="checkbox-inline">الاثنين</label>
                                        <input class="minimal" name="mon" type="checkbox" >
                                        <label class="checkbox-inline">الثلاثاء</label>
                                        <input class="minimal" name="tue" type="checkbox" >
                                        <label class="checkbox-inline">الأربعاء</label>
                                        <input class="minimal" name="wed" type="checkbox" >
                                        <label class="checkbox-inline">الخميس</label>
                                        <input class="minimal" name="thu" type="checkbox" >
                                        <label class="checkbox-inline">الجمعة</label>
                                        <input class="minimal" name="fri" type="checkbox" >
                                    </div>
								</div>
								<div class="col-md-12">
									<br> <a href="{{route('admin.transportations')}}" class="btn btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
	</section>
</div>
<!-- Content page End -->
@endsection