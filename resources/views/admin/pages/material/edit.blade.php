@extends('admin.layouts.master') 
@section('title')
الدروس 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
<section class="content-header">
      <h1>
        <i class="fa fa-arrow-left"></i>
        <span class="semi-bold">الرئيسية</span>
        <small>بيانات الدرس</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href="{{route('admin.materials')}}"> بيانات الدروس</a></li>
        <li class="active">تعديل</li>
      </ol>
    </section>
	<section class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
			<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">تعديل بيانات الدرس</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
					@foreach($materials as $material)
						<form class="mtb-15" action="{{route('admin.materials.edit' , ['id' => $material->id])}}" enctype="multipart/form-data" method="post" onsubmit="return false;">{{ csrf_field() }}
							<div class="row form-row">
								<div class="col-md-6">
									<h4>بيانات الدرس</h4>
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم الدرس</label>
										<input value="{{$material->material_name}}" name="name" class="form-control" type="text">
									</div>



									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">درجه الجزء </label>
										<input value="{{$material->grade}}" name="grade" class="form-control" type="text">
									</div>


									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">اسم الجزء</label>
										<input value="{{$material->percent_name}}" name="percent_name" class="form-control" type="text">
									</div>


										<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">االتفاصيل</label>
										<input value="{{$material->details}}" name="details" class="form-control" type="text">
									</div>
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">درجة النجاح</label>
										<input value="{{$material->success}}" name="success" class="form-control" type="number">
									</div>
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">المستوى</label>
										<select name="level_id" class="select2 pmd-select2 form-control">
											<option value="{{$material->level_id}}" selected>{{$material->level_name}}</option>@foreach($levels as $level)
											<option value="{{$level->id}}">{{$level->level_name}}</option>@endforeach</select>
									</div>
									<div class="col-md-6">
									<div class="form-group">
										<label class="switch">
										<input type="checkbox" id="togBtn" name="active" @if($material->active == 1) checked @endif>
										<div class="slider round">
											<span class="on">نشط</span>
											<span class="off">غير نشط</span>
										</div>
										</label>
									</div>
								</div>
								</div>
								<div class="col-md-6">
									<table id="example1" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>التقدير</th>
												<th>النسبة</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>امتياز</td>
												<td>
													<input name="p1" value="{{$material->p1}}" type="text" size="3">%</td>
											</tr>
											<tr>
												<td>جيد جدا</td>
												<td>
													<input name="p2" value="{{$material->p2}}" type="text" size="3">%</td>
											</tr>
											<tr>
												<td>جيد</td>
												<td>
													<input name="p3" value="{{$material->p3}}" type="text" size="3">%</td>
											</tr>
											<tr>
												<td>مقبول</td>
												<td>
													<input name="p4" value="{{$material->p4}}" type="text" size="3">%</td>
											</tr>
											<tr>
												<td>ضعيف (<small>أقل من</small>)</td>
												<td>
													<input name="p5" value="{{$material->p5}}" type="text" size="3">%</td>
											</tr>
											</tfoot>
									</table>
								</div>
							<div class="col-md-12">
								<br> <a href="{{route('admin.materials')}}" class="btn btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
								<button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
							</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form class="mtb-15" action="{{route('admin.materialDoc.add')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">
					{{ csrf_field() }}
					<div class="col-md-5">
						<div class="box box-warning">
							<div class="box-header with-border">
								<h3 class="box-title"><span class="semi-bold"> ادراج ملفات الدرس</span></h3>
								<div class="box-tools pull-right">
									<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
									<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
									<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
									<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<div class="box-body">
							<div class="form-group custom-inputfile">
                                    <input type="file" name="image2" id="file-7" class="inputfile inputfile-6"/>
                                    <label for="file-7"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>  حمل ملف </strong></label>
                                    <input type="hidden" value="{{$material->id}}" name="material_id">
                                    <input type="hidden" value="materials" name="storage2">
                                </div>
                                	<div class="">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label class="control-label">التفاصيل </label>
										<input name="details" class="form-control" type="textarea">
									</div>
								</div>
								<div class="form-group pmd-textfield pmd-textfield-floating-label">
									<br> <a href="{{route('admin.home')}}" class="btn btn-orange pmd-ripple-effect btn-sm"> الغاء</a>
									<button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="col-md-7">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold"> ملفات الدرس</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>التفاصيل</th>
                                    <th>الملف</th>
                                    <th>العمليات</th>
                                </tr>
                          
                              
                                @foreach($docs as $doc)
                                    <tr>
                                        <td>{{$loop->index + 1}}.</td>
                                            <td>{{$doc->details}}</td>
                                     
                                        <td><a href="{{asset('storage/uploads/org').'/'.$doc->file}}"><i class="fa fa-file-text"></i>    {{$doc->file}}</a></td>
                                        <td>
                                            <a href="{{url('admin/downloadFile1/'.$doc->id)}}" title="تحميل"> <i class="fa fa-download" target="_blank"></i></a>
                                            <a type="submit" class="btndelet" href="{{ route('admin.materialDoc.delete' , ['id' => $doc->id]) }}"  title="حذف"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody></table>
                               
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- End col -->
			</div>
		</div>
			<div class="col-md-7 col-md-offset-2">



		<div class="row">
			<div class="col-md-3 col-md-offset-2">










			<div class="col-md-7">
				</div>
			@endforeach

		</div>
			</div>
			</div>
		</div>

	</section>
</div>


<!-- Content page End -->
@endsection