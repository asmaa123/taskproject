@extends('admin.layouts.master')
@section('title')
الفصول الدراسية 
@endsection
@section('content')
<!-- Content page Start -->
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">الفصول الدراسية</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
                    <div class="box-body">
                        <table id="tables" class="display">
                            <thead>
                            <tr>
                              <th class="num">#</th>
                              <th>الفصل الدراسي</th>
                              <th> السنة الدراسية</th>
                              <th>المصروفات</th>
                               <th> نوع الحلقة</th>
                              <th>العمليات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($seasons as $season)
                            <tr>
                                <td class="num">{{$loop->index + 1}}</td>
                                <td>{{$season->season_name}}</td>
                                <td>{{$season->season_year}}</td>
                                <td>{{$season->price}}</td>
                                 <td>{{$season->type_name}}</td>
                                <td class="action">
                                <!--<a href="{{ route('admin.seasons.edit' , ['id' => $season->id]) }}" class="btn btn-green"> عرض   </a>-->
                                <a href="{{ route('admin.seasons.edit' , ['id' => $season->id]) }}" title="تعديل"> <i class="fa fa-edit"></i>   </a>
                                <a class=" btndelet" href="{{ route('admin.seasons.delete' , ['id' => $season->id]) }}" title="حذف" >
                                    <i class="fa fa-trash"></i>
                                </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
  <!-- Content page End -->
@endsection

