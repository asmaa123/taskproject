@extends('admin.layouts.master')
@section('title')
    الموظفين
@endsection
@section('content')
    <!-- Content page Start -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <i class="fa fa-arrow-left"></i>
                <span class="semi-bold">الرئيسية</span>
                <small>بيانات الموظف</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> الرئيسية</a></li>
                <li><a href="{{route('admin.teachers')}}"> بيانات الموظفين</a></li>
                <li class="active">اضافة</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><span class="semi-bold">اضافة موظف جديد</span></h3>
                            <div class="box-tools pull-right">
                                <a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
                                <a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
                                <a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-body">
                            <form class="mtb-15" action="{{route('admin.teachers.add')}}" enctype="multipart/form-data" method="post" onsubmit="return false;">
                                {{ csrf_field() }}
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <div class="user-img-upload">
                                            <div class="fileUpload user-editimg">
                                                <!--<span><i class="fa fa-camera"></i> اضف</span>-->
                                                <input type='file' id="imgInp" class="upload" name="image" />
                                                <input type="hidden" value="teachers" name="storage" >
                                            </div>
                                            <img src="{{asset('assets/admin/img/add.png')}}" id="blah" class="img-circle" alt="">
                                            <p id="result"></p>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">اسم الموظف</label>
                                            <input name="name" class="form-control" type="text">
                                            <input name="d1" type="hidden" value="Saturday">
                                            <input name="dd1" type="hidden" value="السبت">
                                            <input name="d2" type="hidden" value="Sunday">
                                            <input name="dd2" type="hidden" value="الأحد">
                                            <input name="d3" type="hidden" value="Monday">
                                            <input name="dd3" type="hidden" value="الاثنين">
                                            <input name="d4" type="hidden" value="Tuesday">
                                            <input name="dd4" type="hidden" value="الثلاثاء">
                                            <input name="d5" type="hidden" value="Wednesday">
                                            <input name="dd5" type="hidden" value="الأربعاء">
                                            <input name="d6" type="hidden" value="Thursday">
                                            <input name="dd6" type="hidden" value="الخميس">
                                            <input name="d7" type="hidden" value="Friday">
                                            <input name="dd7" type="hidden" value="الجمعة">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label>الوظيفة</label>
                                            <select class="form-control pmd-select2 select2" name="job_id">
                                                <option></option>
                                                @foreach($jobs as $job)
                                                    <option value="{{$job->id}}">{{$job->job_name}}</option>@endforeach</select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">البريد الالكترونى</label>
                                            <input name="email" type="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">عدد ساعات العمل اليومية</label>
                                            <input name="hours" class="form-control" type="number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">رقم الهاتف</label>
                                            <input name="phone" type="text" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">الراتب الأساسى</label>
                                            <input name="salary" type="number" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">اسم المستخدم</label>
                                            <input name="username" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">كلمة السر</label>
                                            <input name="password" type="password" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6" dir="rtl">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">تاريخ الميلاد</label>
                                            <input name="birth" type="text" class="form-control datepicker">
                                        </div>
                                    </div>
                                    <div class="col-md-6" dir="rtl">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">يوم التوظيف</label>
                                            <input name="first_day" type="text" class="form-control datepicker">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">الرقم المدنى</label>
                                            <input name="national_id" class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="switch">
                                                <input type="checkbox" id="togBtn" name="active" checked>
                                                <div class="slider round">
                                                    <span class="on">نشط</span>
                                                    <span class="off">غير نشط</span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group pmd-textfield pmd-textfield-floating-label">
                                            <label class="control-label">عنوان الموظف</label>
                                            <textarea class="form-control" name="address" rows="3"></textarea>
                                        </div>
                                    </div
                                    
                                
   
   </div>
                                
                        

       
         <div class="row">
    







  <div class="col-md-3">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label> المحافظة</label>
										<select name="id" class="form-control pmd-select2 select2" id="add_town">
											<option></option>
											@foreach(\App\Town::all() as $town)
												<option value="{{$town->id}}">{{$town->town_name}}</option>
											@endforeach
										</select>
									</div>
								</div>
                <div class="col-md-3">
                  <div class="form-group pmd-textfield pmd-textfield-floating-label">
                    <label>المركز</label>
                    <select name="center_id" class="form-control pmd-select2 select2" id="add_center">
                      <option></option>
                    </select>
                  </div>
                </div>
                
               
                <div class="col-md-3">
									<div class="form-group pmd-textfield pmd-textfield-floating-label">
										<label>نوع الحلقة</label>
										<select name="coursetype_id" class="form-control pmd-select2 select2" id="add_type">
											<option></option>
									</select>
									</div>
								</div>
								
				
            
                         <div class="col-md-3">
                  <div class="form-group pmd-textfield pmd-textfield-floating-label">
                    <label>الحلقة</label>
                    <select class="form-control pmd-select2 select2" id="add_course" name="course_id">
                      <option></option>
                    </select>
                  </div>
                </div>
                
                




                    
              
           





    
   </div>
         
                                
                                <!-- /.row -->
                        <!-- /.box-body -->
                        <div class="box-footer"> <a href="{{route('admin.teachers')}}" class="btn btn-orange pmd-ripple-effect btn-sm">  الغاء</a>
                		<button type="submit" class="btn btn-blue addButton pmd-ripple-effect btn-sm">حفظ</button>
                        </div>
                    <!-- /.box -->
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Content page End -->
@endsection