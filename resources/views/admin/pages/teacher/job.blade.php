@extends('admin.layouts.master')
@section('title')
الوظائف
@endsection
@section('content')
<!-- Content page Start -->
  <div class="content-wrapper">
  
    <section class="content">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title"><span class="semi-bold">الوظائف</span></h3>
						<div class="box-tools pull-right">
							<a class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-chevron-down"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-repeat"></i></a>
							<a class="btn btn-box-tool"><i class="fa fa-cog"></i></a>
							<a class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></a>
						</div>
					</div>
					<div class="box-body">
                        <table id="tables" class="display" style="max-width: 100%;">
                            <thead>
                                <tr>
                                    <th class="num">#</th>
                                    <th>اسم الوظيفة</th>
                                    <th>العمليات</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($jobs as $job)
                                <tr>
                                    <td class="num">{{$loop->index + 1}}</td>
                                    <td>{{$job->job_name}}</td>
                                    <td  class="action"> 
                                        <a href="{{ route('admin.jobs.edit' , ['id' => $job->id]) }}" title="تعديل"><i class="fa fa-edit"></i> </a>
                                        <a class="btndelet" href="{{ route('admin.jobs.delete' , ['id' => $job->id]) }}" title="حذف"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
  </div>
  <!-- Content page End -->
@endsection