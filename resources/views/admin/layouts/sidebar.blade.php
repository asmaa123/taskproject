<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image"><img src="{{asset('storage/uploads/users').'/'.Auth::guard('admins')->user()->image}}" class="img-circle" alt=""></div>
      <div class="pull-left info">
        <h3>مرحباً</h3>
        <p>{{Auth::guard('admins')->user()->name}}</p>
        <a href="{{route('admin.profile')}}">الحالة <i class="fas fa-bullseye text-success"></i> أونلاين</a>
      </div>
    </div>

    @if(Auth::guard('admins')->user()->type == 'student' || Auth::guard('admins')->user()->type == 'admin' )

          <?php  $user_id = Auth::guard('admins')->user()->id; ?>

{{--      @if(App\User::checkRole($user_id, 'student'))--}}
{{--          //this part only the student will see it--}}
{{--              @if(User::_can(1, Auth::guard('admins')->user()->id))--}}
{{--              <li><a href="{{route('admin.students')}}"><i class="fas fa-bullseye"></i> عرض الطلاب</a></li>--}}
{{--              @endif--}}
{{--              <li><a href="{{route('admin.students.add')}}"><i class="fas fa-bullseye"></i> اضافة طالب</a></li>--}}
{{--              <li><a href="{{route('admin.students.files')}}"><i class="fas fa-bullseye"></i> ملفات الطلاب</a></li>--}}

{{--      @endif--}}

{{--        @if(App\User::checkRole($user_id, 'Employee'))--}}
{{--          //this part only the Employee will see it--}}

{{--        @endif--}}


      <ul class="sidebar-menu">
      <li class="@if(Route::currentRouteName()=='admin.home') active @endif">
        <a href="{{route('admin.home')}}">
          <i class="fas fa-home"></i>
          <span>لوحة التحكم</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.org') active @endif">
        <a href="{{route('admin.org')}}">
          <i class="fas fa-edit"></i>
          <span>بيانات المؤسسة</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.data') active @endif">
        <a href="{{route('admin.data')}}">
          <i class="fas fa-newspaper"></i>
          <span>وثائق المؤسسة</span>
        </a>
      </li>


      <li class="@if(Route::currentRouteName()=='admin.permission') active @endif">
        <a href="{{route('admin.permission')}}">
          <i class="fas fa-newspaper"></i>
          <span>  صلاحيات المستخدمين  </span>
        </a>
      </li>


      <li class="@if(Route::currentRouteName()=='admin.store') active @endif">
        <a href="{{route('admin.store')}}">
          <i class="fas fa-shopping-cart"></i>
          <span> الخزينة</span>
        </a>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.seasons') active @elseif(Route::currentRouteName()=='admin.seasons.add') active @endif">
        <a href="#">
          <i class="fas fa-book-reader"></i>
          <span>الفصول الدراسية</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.seasons')}}"><i class="fas fa-bullseye"></i> عرض الفصول الدراسية</a></li>
          <li><a href="{{route('admin.seasons.add')}}"><i class="fas fa-bullseye"></i> اضافة فصل دراسي</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.towns') active @elseif(Route::currentRouteName()=='admin.towns.add') active @endif">
        <a href="#">
          <i class="fas fa-globe"></i>
          <span>المحافظات </span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.towns')}}"><i class="fas fa-bullseye"></i> عرض المحافظات</a></li>
          <li><a href="{{route('admin.towns.add')}}"><i class="fas fa-bullseye"></i> اضافة محافظة</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.centers') active @elseif(Route::currentRouteName()=='admin.centers.add') active @endif">
        <a href="#">
          <i class="fas fa-building"></i>
          <span>المراكز </span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.centers')}}"><i class="fas fa-bullseye"></i> عرض المراكز</a></li>
          <li><a href="{{route('admin.centers.add')}}"><i class="fas fa-bullseye"></i> اضافة مركز</a></li>
          <li><a href="{{route('admin.centers.files')}}"><i class="fas fa-bullseye"></i> ملفات المراكز</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.transportations') active @elseif(Route::currentRouteName()=='admin.transportations.add') active @endif">
        <a href="#">
          <i class="fas fa-bus"></i>
          <span>المواصلات </span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.transportations')}}"><i class="fas fa-bullseye"></i> عرض المواصلات</a></li>
          <li><a href="{{route('admin.transportations.add')}}"><i class="fas fa-bullseye"></i> اضافة مواصلة</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.types') active @elseif(Route::currentRouteName()=='admin.types.add') active @endif">
        <a href="#">
          <i class="fas fa-newspaper"></i>
          <span>انواع الحلقات </span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.types')}}"><i class="fas fa-bullseye"></i> عرض انواع الحلقات</a></li>
          <li><a href="{{route('admin.types.add')}}"><i class="fas fa-bullseye"></i> اضافة نوع جديد</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.courses') active @elseif(Route::currentRouteName()=='admin.courses.add') active @endif">
        <a href="#">
          <i class="fas fa-chalkboard-teacher"></i>
          <span> الحلقات</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.courses')}}"><i class="fas fa-bullseye"></i> عرض الحلقات</a></li>
          <li><a href="{{route('admin.courses.add')}}"><i class="fas fa-bullseye"></i> اضافة حلقة</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.levels') active @elseif(Route::currentRouteName()=='admin.levels.add') active @endif">
        <a href="#">
          <i class="fas fa-user-graduate"></i>
          <span> المستويات</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.levels')}}"><i class="fas fa-bullseye"></i> عرض المستويات</a></li>
          <li><a href="{{route('admin.levels.add')}}"><i class="fas fa-bullseye"></i> اضافة مستوى</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.materials') active @elseif(Route::currentRouteName()=='admin.materials.add') active @elseif(Route::currentRouteName()=='admin.materials.files') active @elseif(Route::currentRouteName()=='admin.materials.grades') active @endif">
        <a href="#">
          <i class="fas fa-file-alt"></i>
          <span>الدروس</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.materials')}}"><i class="fas fa-bullseye"></i> عرض الدروس</a></li>
          <li><a href="{{route('admin.materials.add')}}"><i class="fas fa-bullseye"></i> اضافة درس</a></li>
          <li><a href="{{route('admin.materials.files')}}"><i class="fas fa-bullseye"></i> ملفات الدروس</a></li>
          <li><a href="{{route('admin.materials.grades')}}"><i class="fas fa-bullseye"></i> اضافة أجزاء الدروس</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.teachers') active @elseif(Route::currentRouteName()=='admin.teachers.add') active @endif">
        <a href="#">
          <i class="fas fa-user-circle"></i>
          <span>الموظفين</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.teachers')}}"><i class="fas fa-bullseye"></i> عرض الموظفين</a></li>
          <li><a href="{{route('admin.teachers.add')}}"><i class="fas fa-bullseye"></i> اضافة موظف</a></li>
          <li><a href="{{route('admin.teachers.files')}}"><i class="fas fa-bullseye"></i> ملفات الموظفين</a></li>
          <li><a href="{{route('admin.teachers.attend')}}"><i class="fas fa-bullseye"></i> تسجيل الحضور</a></li>
          <li><a href="{{route('admin.teachers.leave')}}"><i class="fas fa-bullseye"></i> تسجيل الانصراف</a></li>
          <li><a href="{{route('admin.jobs')}}"><i class="fas fa-bullseye"></i> الوظائف</a></li>
          <li><a href="{{route('admin.jobs.add')}}"><i class="fas fa-bullseye"></i> اضافة وظيفة</a></li>
          <li><a href="{{route('admin.salary.add')}}"><i class="fas fa-bullseye"></i> صرف سلفة</a></li>
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.students') active @elseif(Route::currentRouteName()=='site.students.pendingadds') active @elseif(Route::currentRouteName()=='site.students.pendingedits') active @elseif(Route::currentRouteName()=='admin.students.add') active @endif">
        <a href="#">
          <i class="fas fa-graduation-cap"></i>
          <span>الطلاب</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.students')}}"><i class="fas fa-bullseye"></i> عرض الطلاب</a></li>
          <li><a href="{{route('admin.students.add')}}"><i class="fas fa-bullseye"></i> اضافة طالب</a></li>
          <li><a href="{{route('admin.students.files')}}"><i class="fas fa-bullseye"></i> ملفات الطلاب</a></li>
          <li><a href="{{route('admin.students.pendingadds')}}"><i class="fas fa-bullseye"></i> طلبات الالتحاق</a></li>
          <li><a href="{{route('admin.students.pendingedits')}}"><i class="fas fa-bullseye"></i> طلبات التعديل</a></li>
          <!--<li><a href="{{route('admin.students.payment')}}"><i class="fas fa-bullseye"></i> دفع الاشتراك</a></li>
          <li><a href="{{route('admin.students.absent')}}"><i class="fas fa-bullseye"></i> تسجيل الحضور</a></li>
          <li><a href="{{route('admin.students.attend')}}"><i class="fas fa-bullseye"></i> تقارير الحضور</a></li>
          
          <li><a href="{{route('admin.students.report')}}"><i class="fas fa-bullseye"></i> تقارير الطلاب</a></li>-->
        </ul>
      </li>
      <li class="treeview @if(Route::currentRouteName()=='admin.guardians') active @elseif(Route::currentRouteName()=='admin.guardians.add') active @endif">
        <a href="#">
          <i class="fas fa-users"></i>
          <span>أولياء الأمور</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.guardians')}}"><i class="fas fa-bullseye"></i> عرض أولياء الأمور</a></li>
          <li><a href="{{route('admin.guardians.add')}}"><i class="fas fa-bullseye"></i> اضافة ولى أمر</a></li>
        </ul>
      </li>      
      <li class="treeview">
        <a href="#">
          <i class="fas fa-newspaper"></i>
          <span>التقارير</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.reports.absent')}}"><i class="fas fa-bullseye"></i> تقارير حضور الطلاب</a></li>
          <li><a href="{{route('admin.reports.grades')}}"><i class="fas fa-bullseye"></i> تقارير درجات الطلاب</a></li>
          <li><a href="{{route('admin.reports.counts')}}"><i class="fas fa-bullseye"></i> تقارير حسابات الطلاب</a></li>
          <li><a href="{{route('admin.reports.Scounts')}}"><i class="fas fa-bullseye"></i> تقارير حسابات الطلاب بايجاز</a></li>
          <li><a href="{{route('admin.reports.attend')}}"><i class="fas fa-bullseye"></i> تقارير حضور الموظفين</a></li>
          <li><a href="{{route('admin.reports.salaries')}}"><i class="fas fa-bullseye"></i> تقارير مرتبات الموظفين</a></li>
          <li><a href="{{route('admin.reports.students')}}"><i class="fas fa-bullseye"></i> تقارير عدد حلقات طلاب المراكز</a></li>
          <li><a href="{{route('admin.reports.levelss')}}"><i class="fas fa-bullseye"></i> تقارير بااعداد الطلاب بالمستويات </a></li>
         <li><a href="{{route('admin.reports.teacher')}}"><i class="fas fa-bullseye"></i> تقارير حساب ايام عمل المعلمين</a></li>
         <li><a href="{{route('admin.reports.Exemptstudent')}}"><i class="fas fa-bullseye"></i> تقارير الطلاب المعفين من الرسوم</a></li>    
         <li><a href="{{route('admin.reports.studentss')}}"><i class="fas fa-bullseye"></i>تقارير بااعداد الطلاب والمراكز </a></li>
         <li><a href="{{route('admin.reports.studentcourse')}}"><i class="fas fa-bullseye"></i>تقارير طالب حلقه الكتاب   </a></li>
        
         <li><a href="{{route('admin.reports.datacenter')}}"><i class="fas fa-bullseye"></i>تقارير بيانات المراكز  </a></li>
        <li><a href="{{route('admin.reports.levelstudents')}}"><i class="fas fa-bullseye"></i>تقارير بمستويات الطلاب بحلقات القران   </a></li>
       <li><a href="{{route('admin.reports.levelstudents')}}"><i class="fas fa-bullseye"></i>تقارير النتاءج الحلقات طلاب الكتاب   </a></li>
        <li><a href="{{route('admin.reports.studentdatacourse')}}"><i class="fas fa-bullseye"></i>تقارير بيانات طالب حلقه تحفيظ القران  </a></li>
        <li><a href="{{route('admin.reports.parts')}}"><i class="fas fa-bullseye"></i>تقارير بااعداد الاحزاب المنجزه  </a></li>  
         <li><a href="{{route('admin.reports.financialclaims')}}"><i class="fas fa-bullseye"></i>  تقارير بكشف المطالبات الماليه</a></li>
         <li><a href="{{route('admin.reports.bookstudents')}}"><i class="fas fa-bullseye"></i>  تقاير النهاءي لحلقه طلاب الكتاب </a></li>
          <li><a href="{{route('admin.reports.differentparties')}}"><i class="fas fa-bullseye"></i>تقارير باعداد الطلاب وفق الاحزاب </a></li>
    
          
        <li><a href="{{route('admin.reports.passlevels')}}"><i class="fas fa-bullseye"></i>  شهاده اجتياز مستوي   </a></li>
         <li><a href="{{route('admin.reports.graduationcertificate')}}"><i class="fas fa-bullseye"></i>شهاده تخرج   </a></li>
         <li><a href="{{route('admin.reports.finaltest')}}"><i class="fas fa-bullseye"></i>شهاده اختبار نهاءي  </a></li>
            
          <li><a href="{{route('admin.reports.skilldegree')}}"><i class="fas fa-bullseye"></i>شهاده درجه الماهر  </a></li>
            
        </ul>
      </li>
      
    </ul>
    @elseif(Auth::guard('admins')->user()->type == 'teacher')
    <ul class="sidebar-menu">
      <li class="@if(Route::currentRouteName()=='admin.home') active @endif">
        <a href="{{route('admin.home')}}">
          <i class="fas fa-home"></i>
          <span>لوحة التحكم</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fas fa-newspaper"></i>
          <span>التقارير</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.reports.attend')}}"><i class="fas fa-bullseye"></i> تقارير حضور الموظفين</a></li>
        </ul>
      </li>
    </ul>
    @elseif(Auth::guard('admins')->user()->type == 'accountant')
    <ul class="sidebar-menu">
      <li class="@if(Route::currentRouteName()=='admin.home') active @endif">
        <a href="{{route('admin.home')}}">
          <i class="fas fa-home"></i>
          <span>لوحة التحكم</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.org') active @endif">
        <a href="{{route('admin.org')}}">
          <i class="fas fa-edit"></i>
          <span>بيانات المؤسسة</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.data') active @endif">
        <a href="{{route('admin.data')}}">
          <i class="fas fa-newspaper"></i>
          <span>وثائق المؤسسة</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.store') active @endif">
        <a href="{{route('admin.store')}}">
          <i class="fas fa-shopping-cart"></i>
          <span> الخزينة</span>
        </a>
      </li>
      <li class="@if(Route::currentRouteName()=='admin.student.store') active @endif">
        <a href="{{route('admin.student.store')}}">
          <i class="fas fa-shopping-cart"></i>
          <span> تسجيل بيانات الطلاب</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fas fa-newspaper"></i>
          <span>التقارير</span>
          <i class="fas fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.reports.absent')}}"><i class="fas fa-bullseye"></i> تقارير حضور الطلاب</a></li>
          <li><a href="{{route('admin.reports.grades')}}"><i class="fas fa-bullseye"></i> تقارير درجات الطلاب</a></li>
          <li><a href="{{route('admin.reports.counts')}}"><i class="fas fa-bullseye"></i> تقارير حسابات الطلاب</a></li>
          <li><a href="{{route('admin.reports.Scounts')}}"><i class="fas fa-bullseye"></i> تقارير حسابات الطلاب بايجاز</a></li>
          <li><a href="{{route('admin.reports.attend')}}"><i class="fas fa-bullseye"></i> تقارير حضور الموظفين</a></li>
          <li><a href="{{route('admin.reports.salaries')}}"><i class="fas fa-bullseye"></i> تقارير مرتبات الموظفين</a></li>
        </ul>
      </li>
      
    </ul>
    @endif
  </section>
  <!-- Sidebar End -->
</aside>