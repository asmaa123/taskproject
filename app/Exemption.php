<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exemption extends Model
{
    protected $fillable = ['reason' ,'ratio' ];

    public function students(){
        return $this->hasMany(Student::class);
    }
    
     
}
