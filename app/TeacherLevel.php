<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherLevel extends Model
{
    protected $fillable = ['teacher_id' ,'level_id'];

    public function level(){
        return $this->belongsTo(Level::class ,'level_id');
    }

    public function teacher(){
        return $this->belongsTo(Teacher::class ,'teacher_id');
    }
}
