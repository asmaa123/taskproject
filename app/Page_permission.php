<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page_permission extends Model 
{

    protected $table = 'page_permission';
    public $timestamps = true;
    protected $fillable = array('page_id', 'permission_id');

}