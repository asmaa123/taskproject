<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    protected $fillable = ['teacher_id' ,'month' ,'year','day','part'];
    
    
    
   public function type(){
        return $this->belongsTo(Type::class );
    }

     public function students(){
        return $this->hasMany(Student::class);
    }

     public function Material(){
        return $this->belongsTo(Material::class,'material_id' );
    }
    
}
