<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherDocument extends Model
{
    protected $fillable = ['teacher_id','file'];

}
