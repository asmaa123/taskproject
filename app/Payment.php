<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['student_id' ,'amount','remain','notes','code'];
    
    
     public function students(){
        return $this->hasMany(Student::class ,'student_id');
    }
}
