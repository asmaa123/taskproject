<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CenterDocument extends Model
{
    protected $fillable = ['center_id','file','type'];

}
