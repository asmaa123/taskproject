<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['course_name' ,'center_id','type_id' ,'max_num','time','price','active'];

    public function details(){
        return $this->hasMany(CourseMaterial::class ,'course_id');
    }

    public function levels(){
        return $this->hasMany(CourseLevel::class ,'course_id');
    }

    public function times(){
        return $this->hasMany(CourseTime::class ,'course_id');
    }
  public function students(){
        return $this->hasMany(Student::class ,'course_id');
    }

  public function Absents(){
        return $this->hasMany(Absent::class );
    }
    
     public function teachers(){
        return $this->hasMany(Teacher::class,'course_id' );
    }
     public function materials(){
        return $this->hasMany(Material::class,'course_id' );
    }
    
    
      public function type(){
        return $this->belongsto(Type::class,'type_id' );
    }
    
    public function CourseType(){
        return $this->belongsto(CourseType::class,'coursetype_id' );
    }
    
      public function center(){
        return $this->belongsto(Center::class,'center_id' );
    } 
}
