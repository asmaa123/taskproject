<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseLevel extends Model
{
    protected $fillable = ['type_id' ,'level_id'];

    public function level(){
        return $this->belongsTo(Level::class ,'level_id');
    }

    public function course(){
        return $this->belongsTo(Course::class ,'type_id');
    }
}
