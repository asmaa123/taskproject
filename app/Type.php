<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\language;

class Type extends Model
{
    protected $fillable = ['name'];

    public function doc(){
        return $this->hasMany(Doc::class ,'type_id');
    }
    
     public function parts(){
        return $this->hasMany(Part::class ,'part_id');
    }

  public function levels(){
        return $this->hasMany('App\Level' );
    }
    
    
    
      public function courses(){
        return $this->hasMany(Course::class);
    }
    
  public function center(){
        return $this->belongto(Center::class,'center_id');
    }
    
}
