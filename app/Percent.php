<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Percent extends Model
{
    protected $fillable = ['percent_name' ,'grade' ,'material_id'];


    public function Material(){
        return $this->belongsTo(Material::class,'material_id' );
    }
}
