<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseTime extends Model
{
    protected $fillable = ['course_id' ,'day','time_from','time_to'];

    public function course(){
        return $this->belongsTo(Course::class ,'course_id');
    }
}
