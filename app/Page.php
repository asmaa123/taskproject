<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model 
{

    protected $table = 'pages';
    public $timestamps = true;
    protected $fillable = array('page_name', 'page_route');

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

}