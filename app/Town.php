<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['town_name','active'];
    
    
    
    
       public function centers (){
        return $this->hasMany('App\Center');
    }
    
    
    
}
