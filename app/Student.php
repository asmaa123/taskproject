<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['image','student_name','birth','age','gender','national_id','address','email','first_day' ,'level_id' ,'center_id','guardian_id','nationality','transportation','year','season_id','active'];
    
    public function details(){
        return $this->hasMany(StudentMaterial::class ,'student_id');
    }

    public function archives(){
        return $this->hasMany(StudentArchive::class ,'student_id');
    }

    public function courses(){
        return $this->hasMany(StudentCourse::class ,'student_id');
    }

    public function absent(){
        return $this->hasMany(Absent::class ,'student_id');
    }

    public function level(){
        return $this->belongsTo(Level::class ,'level_id');
    }
    
     public function part(){
        return $this->belongsTo(Part::class );
    } 
    
    
      public function guardian(){
        return $this->belongsTo(Guardian::class ,'guardian_id');
    }
    
      public function attend(){
        return $this->belongsto(Attend::class );
    }
  public function counts(){
        return $this->hasMany(Count::class );
    }
    
     public function course(){
        return $this->belongsTo(Course::class);
    }
    
    public function payments(){
        return $this->hasMany(Payment::class );
    }
      public function documents(){
        return $this->belongtoMany(Doc::class);
    }
    
     public function studentGrade(){
        return $this->belongsTo(StudentGrade::class ,'studentgrade_id');
    }
    
     public function center(){
        return $this->belongsTo(Center::class,'center_id' );
    } 
    
       public function material(){
        return $this->belongsTo(Material::class);
    }
    
    
        public function exemption(){
        return $this->belongsTo(Exemption::class,'exemption_id' );
    }
    
     
        public function season(){
        return $this->belongsTo(Season::class,'season_id' );
    }
    
    
}
