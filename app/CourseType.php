<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseType extends Model
{
    protected $fillable = ['type_name','active','type_id','course_id'];

    public function Courses(){
        return $this->hasMany(Course::class ,'type_id');
    }


  public function seasons(){
        return $this->hasMany(Season::class);
    }
 public function center(){
        return $this->belongsTo(Center::class);
    }
    
      public function teachers(){
        return $this->hasMany(Teacher::class);
    }
    
       public function levels(){
        return $this->hasMany(Level::class);
    }


}
