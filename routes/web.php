<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/test101', function(){

    $user_id = Auth::guard('admins')->user()->id;
    dd();


});*/
Route::get('/lang/{locale}', ['as' => 'site.lang', 'uses' => 'LangController@postIndex']);
Route::get('/error', ['as' => 'site.error', 'uses' => 'Controller@error']);
// Route::get('/chartdata', function(){
//     return ['value' => rand(50,100)];
// })->name('chartdata');
// Route::get('/markAsRead', function(){
//     Auth::guard('admins')->user()->unreadNotifications->markAsRead();
//     return redirect()->back();
// })->name('markAsRead');

Route::get('/attachStudentPermissions', function(){
    \App\Helpers\PermissonsHelper::attachStudentPermissions();
})->name('attachStudentPermissions');

Route::group(['namespace' => 'Site'], function () {
    Route::get('/', 'Auth\AuthController@getIndex');
    Route::get('/login', 'Auth\AuthController@getIndex');
    Route::post('/login', ['as' => 'site.login', 'uses' => 'Auth\AuthController@postLogin']);
    Route::get('/logout', ['as' => 'site.logout', 'uses' => 'Auth\AuthController@logout']);

    Route::group(['middleware' => 'auth.site'], function () {
        // Route::get('/phone', ['as' => 'member.phone', 'uses' => 'Auth\AuthController@phone']);
        // Route::Post('/verify', ['as' => 'phone.verify', 'uses' => 'Auth\AuthController@verify']);
        

        Route::get('/', ['as' => 'site.home', 'uses' => 'HomeController@getIndex']);
        Route::get('/profile', ['as' => 'site.profile', 'uses' => 'HomeController@profile']);
        Route::post('/profile', ['as' => 'site.profile.edit', 'uses' => 'HomeController@editProfile']);
        Route::post('/profileimg', ['as' => 'site.profile.image', 'uses' => 'HomeController@editProfileImage']);
        Route::get('/lock', ['as' => 'site.lock', 'uses' => 'HomeController@lock']);
        Route::post('/lock', ['as' => 'site.back', 'uses' => 'HomeController@back']);

        Route::group(['prefix' => 'students'], function () {
            Route::get('/', ['as' => 'site.students', 'uses' => 'StudentsController@getIndex']);
            Route::get('/add', ['as' => 'site.students.add', 'uses' => 'StudentsController@getAdd']);
            Route::post('/add', ['as' => 'site.students.add', 'uses' => 'StudentsController@insert']);
            Route::get('/edit/{id}', ['as' => 'site.students.edit', 'uses' => 'StudentsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'site.students.edit', 'uses' => 'StudentsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'site.students.delete', 'uses' => 'StudentsController@delete']);
            
        });

        Route::get('/ajax-course', ['uses' => 'HomeController@getCourse']);
        Route::get('/ajax-student', ['uses' => 'HomeController@getStudent']);
        Route::get('/ajax-material', ['uses' => 'HomeController@getMaterial']);
        Route::get('/ajax-type', ['uses' => 'HomeController@getType']);
        Route::get('/ajax-percent', ['uses' => 'HomeController@getPercent']);
        Route::post('/addGrade', ['as' => 'site.grades.add', 'uses' => 'HomeController@grades']);
        Route::get('/ajax-courses', ['uses' => 'HomeController@getCourses']);
        Route::get('/ajax-students', ['uses' => 'HomeController@getStudents']);
        Route::get('/ajax-materials', ['uses' => 'HomeController@getMaterials']);
        Route::get('/ajax-percents', ['uses' => 'HomeController@getPercents']);
        Route::get('/ajax-smaterial', ['uses' => 'HomeController@getSMaterial']);
        Route::get('/ajax-spercent', ['uses' => 'HomeController@getSPercent']);
        Route::get('/ajax-from', ['uses' => 'HomeController@getFrom']);
        Route::get('/ajax-to', ['uses' => 'HomeController@getTo']);
        Route::get('/ajax-student', ['uses' => 'HomeController@getStud']);
        Route::get('/ajax-group', ['uses' => 'HomeController@getGroups']);
        Route::post('/addAbsent', ['as' => 'site.absent.add', 'uses' => 'HomeController@absent']);
        Route::get('/ajax-dto', ['uses' => 'HomeController@dto']);
        Route::get('/ajax-dfrom', ['uses' => 'HomeController@dfrom']);
        Route::get('/ajax-dates', ['uses' => 'HomeController@dates']);
        Route::get('/ajax-studs', ['uses' => 'HomeController@studs']);
        Route::get('/ajax-pcourse', ['uses' => 'HomeController@getPayCourse']);
        Route::get('/ajax-pstudent', ['uses' => 'HomeController@getPayStudent']);
        Route::get('/ajax-materialprice', ['uses' => 'HomeController@getMaterialPrice']);
        Route::get('/ajax-date', ['uses' => 'HomeController@date']);
        Route::get('/ajax-process', ['uses' => 'HomeController@process']);
        Route::get('/ajax-month', ['uses' => 'HomeController@month']);
        Route::post('/payment', ['as' => 'site.payms.add', 'uses' => 'HomeController@payms']);
        Route::get('/payment', ['as' => 'site.payms', 'uses' => 'HomeController@getIndex']);
        Route::get('/pay/{id}', ['as' => 'site.student.pay', 'uses' => 'HomeController@getPay']);
        Route::post('/pay/{id}', ['as' => 'site.student.pay', 'uses' => 'HomeController@postPay']);
        Route::get('/confirm/{id}/{code}/{amount}', ['as' => 'site.student.confirm', 'uses' => 'HomeController@confirm']);
    });
    
});
 Route::get('/search', 'ReportsController@search');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::get('/', 'AuthController@getIndex');
        Route::get('/login', 'AuthController@getIndex');
        Route::post('/login', 'AuthController@postLogin')->name('admin.login');
        Route::get('/logout', 'AuthController@getLogout')->name('admin.logout');
    });


    Route::group(['middleware' => 'auth.admin'], function () {
        Route::get('/', ['as' => 'admin.home', 'uses' => 'HomeController@getIndex']);
        Route::get('/store', ['as' => 'admin.store', 'uses' => 'ReportsController@store']);
        Route::get('/student', ['as' => 'admin.student.store', 'uses' => 'StudentController@getIndex']);
        Route::post('/student', ['as' => 'admin.student.create', 'uses' => 'StudentController@storeData']);
        //Route::get('/error', ['as' => 'admin.error', 'uses' => 'HomeController@error']);
        Route::get('/fetchcenters', ['as' => 'ajaxdata.fetchcenters', 'uses' => 'StudentController@fetchcenters']);
        Route::get('/fetchseasons', ['as' => 'ajaxdata.fetchseasons', 'uses' => 'StudentController@fetchseasons']);
           Route::get('/admin/ajax_town_with_center/{town_id}', 'TypesController@getcenter');
        Route::get('/ajax_type_with_courses/{type_id}', 'TypesController@getTypeWithCoursesInAjax');
        Route::get('/ajax_type_with_centers/{center_id}', 'TypesController@getTypeWithCenterInAjax');
        
         Route::get('/ajax_type_with_levels/{type_id}', 'TypesController@getTypeWithlevelInAjax');
        Route::get('/lock', ['as' => 'admin.lock', 'uses' => 'HomeController@lock']);
        Route::post('/lock', ['as' => 'admin.back', 'uses' => 'HomeController@back']);
        Route::post('/edit', ['as' => 'admin.org.edit', 'uses' => 'HomeController@postEdit']);
        Route::get('/data', ['as' => 'admin.data', 'uses' => 'DataController@getData']);
        Route::get('/downloadFile/{id}',[ 'uses' => 'DataController@downloadFile']);
        Route::get('/downloadFile1/{id}',[ 'uses' => 'DataController@downloadFile1']);
        Route::post('/add', ['as' => 'admin.doc.add', 'uses' => 'DataController@add']);
        Route::get('/delete/{id}', ['as' => 'admin.doc.delete', 'uses' => 'DataController@delete']);
        Route::get('/ajax-course', ['uses' => 'HomeController@getCourse']);
        Route::get('/ajax-student', ['uses' => 'HomeController@getStudent']);
        Route::get('/ajax-material', ['uses' => 'HomeController@getMaterial']);
        Route::get('/ajax-percent', ['uses' => 'HomeController@getPercent']);
        Route::post('/addGrade', ['as' => 'admin.grades.add', 'uses' => 'HomeController@grades']);
        Route::get('/ajax-courses', ['uses' => 'HomeController@getCourses']);
        Route::get('/ajax-students', ['uses' => 'HomeController@getStudents']);
        Route::get('/ajax-materials', ['uses' => 'HomeController@getMaterials']);
        Route::get('/ajax-percents', ['uses' => 'HomeController@getPercents']);
        Route::get('/ajax-smaterial', ['uses' => 'HomeController@getSMaterial']);
        Route::get('/ajax-spercent', ['uses' => 'HomeController@getSPercent']);
        Route::get('/ajax-from', ['uses' => 'HomeController@getFrom']);
        Route::get('/ajax-to', ['uses' => 'HomeController@getTo']);
        Route::get('/ajax-student', ['uses' => 'HomeController@getStud']);
        Route::get('/ajax-group', ['uses' => 'HomeController@getGroups']);
        Route::post('/addAbsent', ['as' => 'admin.absent.add', 'uses' => 'HomeController@absent']);
        Route::get('/ajax-dto', ['uses' => 'HomeController@dto']);
        Route::get('/ajax-dfrom', ['uses' => 'HomeController@dfrom']);
        Route::get('/ajax-dates', ['uses' => 'HomeController@dates']);
        Route::get('/ajax-studs', ['uses' => 'HomeController@studs']);
        Route::get('/ajax-pcourse', ['uses' => 'HomeController@getPayCourse']);
        Route::get('/ajax-pstudent', ['uses' => 'HomeController@getPayStudent']);
        Route::get('/ajax-materialprice', ['uses' => 'HomeController@getMaterialPrice']);
        Route::get('/ajax-date', ['uses' => 'HomeController@date']);
        Route::get('/ajax-process', ['uses' => 'HomeController@process']);
        Route::get('/ajax-month', ['uses' => 'HomeController@month']);
        Route::post('/pay', ['as' => 'admin.payms.add', 'uses' => 'HomeController@payms']);
        Route::get('/ajax-salary', ['uses' => 'TeachersController@getAjaxSalary']);
        Route::get('/ajax-days', ['uses' => 'TeachersController@getAjaxDays']);
        Route::get('/ajax-attendSalary', ['uses' => 'TeachersController@attendSalary']);
        Route::get('/ajax-timeSalary', ['uses' => 'TeachersController@timeSalary']);
        Route::get('/ajax-parts', ['uses' => 'TeachersController@getAjaxParts']);
        Route::post('/addSalary', ['as' => 'admin.salaries.add', 'uses' => 'TeachersController@insertSalary']);
        Route::get('/ajax-add-course', ['uses' => 'HomeController@getAddCourse']);
        Route::get('/ajax-add-student', ['uses' => 'HomeController@getAddStudent']);
        Route::get('/ajax-add-material', ['uses' => 'HomeController@getAddMaterial']);
        Route::get('/ajax-add-level', ['uses' => 'HomeController@getAddLevel']);
        Route::post('/addnewstudent', ['as' => 'admin.addnewstudent', 'uses' => 'HomeController@addnewstudent']);
        Route::get('/ajax-new-course', ['uses' => 'HomeController@getNewCourse']);
        Route::get('/ajax-new-teacher', ['uses' => 'HomeController@getNewTeacher']);
        Route::get('/ajax-new-material', ['uses' => 'HomeController@getNewMaterial']);
        Route::get('/ajax-new-level', ['uses' => 'HomeController@getNewLevel']);
        Route::post('/addnewteacher', ['as' => 'admin.addnewteacher', 'uses' => 'HomeController@addnewteacher']);
        Route::get('/ajax-getnew-course', ['uses' => 'HomeController@getNCourse']);
        Route::get('/ajax-getnew-material', ['uses' => 'HomeController@getNMaterial']);
        Route::get('/ajax-getnew-level', ['uses' => 'HomeController@getNLevel']);
        Route::post('/addcoursematerial', ['as' => 'admin.addcoursematerial', 'uses' => 'HomeController@addcoursematerial']);

        Route::group(['prefix' => 'org'], function () {
            Route::get('/', ['as' => 'admin.org', 'uses' => 'OrgController@getIndex']);
            Route::post('/add', ['as' => 'admin.org.edit', 'uses' => 'OrgController@postEdit']);
        });

        Route::group(['prefix' => 'centers'], function () {
            Route::get('/', ['as' => 'admin.centers', 'uses' => 'CentersController@getIndex']);
            Route::get('/add', ['as' => 'admin.centers.add', 'uses' => 'CentersController@getAdd']);
            Route::post('/add', ['as' => 'admin.centers.add', 'uses' => 'CentersController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.centers.edit', 'uses' => 'CentersController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.centers.edit', 'uses' => 'CentersController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.centers.delete', 'uses' => 'CentersController@delete']);
            Route::get('/files', ['as' => 'admin.centers.files', 'uses' => 'CentersController@files']);
             Route::get('/downloadFile/{id}',[ 'uses' => 'CentersController@downloadFile']);
            Route::post('/addDoc', ['as' => 'admin.centerDoc.add', 'uses' => 'CentersController@addDoc']);
            Route::get('/deleteDoc/{id}', ['as' => 'admin.centerDoc.delete', 'uses' => 'CentersController@deleteDoc']);
        });

        Route::group(['prefix' => 'guardians'], function () {
            Route::get('/', ['as' => 'admin.guardians', 'uses' => 'GuardiansController@getIndex']);
            Route::get('/add', ['as' => 'admin.guardians.add', 'uses' => 'GuardiansController@getAdd']);
            Route::post('/add', ['as' => 'admin.guardians.add', 'uses' => 'GuardiansController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.guardians.edit', 'uses' => 'GuardiansController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.guardians.edit', 'uses' => 'GuardiansController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.guardians.delete', 'uses' => 'GuardiansController@delete']);
            Route::get('/getcoursesdata', ['as' => 'ajaxdata.getcoursesdata', 'uses' => 'GuardiansController@getcoursesdata']);
            Route::get('/getcentersdata', ['as' => 'ajaxdata.getcentersdata', 'uses' => 'GuardiansController@getcentersdata']);
            Route::get('/getlevelsdata', ['as' => 'ajaxdata.getlevelsdata', 'uses' => 'GuardiansController@getlevelsdata']);
            Route::get('/getmaterialsdata', ['as' => 'ajaxdata.getmaterialsdata', 'uses' => 'GuardiansController@getmaterialsdata']);
            Route::get('/getcoursesdataa', ['as' => 'ajaxdata.getcoursesdataa', 'uses' => 'GuardiansController@getcoursesdataa']);
        });

        Route::group(['prefix' => 'seasons'], function () {
            Route::get('/', ['as' => 'admin.seasons', 'uses' => 'SeasonsController@getIndex']);
            Route::get('/add', ['as' => 'admin.seasons.add', 'uses' => 'SeasonsController@getAdd']);
            Route::post('/add', ['as' => 'admin.seasons.add', 'uses' => 'SeasonsController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.seasons.edit', 'uses' => 'SeasonsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.seasons.edit', 'uses' => 'SeasonsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.seasons.delete', 'uses' => 'SeasonsController@delete']);
        });

        Route::group(['prefix' => 'towns'], function () {
        
       
       
            Route::get('/', ['as' => 'admin.towns', 'uses' => 'TownsController@getIndex']);
            Route::get('/add', ['as' => 'admin.towns.add', 'uses' => 'TownsController@getAdd']);
            Route::post('/add', ['as' => 'admin.towns.add', 'uses' => 'TownsController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.towns.edit', 'uses' => 'TownsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.towns.edit', 'uses' => 'TownsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.towns.delete', 'uses' => 'TownsController@delete']);
        });

        Route::group(['prefix' => 'types'], function () {
            Route::get('/', ['as' => 'admin.types', 'uses' => 'TypesController@getIndex']);
            Route::get('/add', ['as' => 'admin.types.add', 'uses' => 'TypesController@getAdd']);
            Route::post('/add', ['as' => 'admin.types.add', 'uses' => 'TypesController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.types.edit', 'uses' => 'TypesController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.types.edit', 'uses' => 'TypesController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.types.delete', 'uses' => 'TypesController@delete']);
        });

        Route::group(['prefix' => 'students'], function () {
            Route::get('/', ['as' => 'admin.students', 'uses' => 'StudentsController@getIndex']);
            Route::get('/count/{id}', ['as' => 'admin.students.count', 'uses' => 'StudentsController@count']);
            Route::get('/add', ['as' => 'admin.students.add', 'uses' => 'StudentsController@getAdd']);
            Route::get('/downloadFile/{id}',[ 'uses' => 'StudentsController@downloadFile']);
            Route::post('/add', ['as' => 'admin.students.add', 'uses' => 'StudentsController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.students.edit', 'uses' => 'StudentsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.students.edit', 'uses' => 'StudentsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.students.delete', 'uses' => 'StudentsController@delete']);
            Route::post('/addDoc', ['as' => 'admin.studentDoc.add', 'uses' => 'StudentsController@addDoc']);
            Route::get('/deleteDoc/{id}', ['as' => 'admin.studentDoc.delete', 'uses' => 'StudentsController@deleteDoc']);
            Route::post('/pay', ['as' => 'admin.students.pay', 'uses' => 'StudentsController@pay']);
            Route::get('/mdelete/{id}', ['as' => 'admin.smaterial.delete', 'uses' => 'StudentsController@mdelete']);
            Route::get('/cdelete/{id}', ['as' => 'admin.scourse.delete', 'uses' => 'StudentsController@cdelete']);
            Route::get('/absent', ['as' => 'admin.students.absent', 'uses' => 'StudentsController@getAbsent']);
            Route::get('/attend', ['as' => 'admin.students.attend', 'uses' => 'StudentsController@attend']);
            Route::get('/files', ['as' => 'admin.students.files', 'uses' => 'StudentsController@files']);
            Route::get('/payment', ['as' => 'admin.students.payment', 'uses' => 'StudentsController@payment']);
            Route::get('/report', ['as' => 'admin.students.report', 'uses' => 'StudentsController@report']);
            Route::get('/pendingadds', ['as' => 'admin.students.pendingadds', 'uses' => 'StudentController@getpendingadd']);
            Route::post('/pendingadd/{id}', ['as' => 'admin.students.pendingadd', 'uses' => 'StudentController@postpendingadd']);
            Route::get('/pendingedits', ['as' => 'admin.students.pendingedits', 'uses' => 'StudentController@getpendingedit']);
            Route::post('/pendingedit/{id}', ['as' => 'admin.students.pendingedit', 'uses' => 'StudentController@postpendingedit']);
        });

        Route::group(['prefix' => 'transportations'], function () {
            Route::get('/', ['as' => 'admin.transportations', 'uses' => 'TransportationsController@getIndex']);
            Route::get('/add', ['as' => 'admin.transportations.add', 'uses' => 'TransportationsController@getAdd']);
            Route::post('/add', ['as' => 'admin.transportations.add', 'uses' => 'TransportationsController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.transportations.edit', 'uses' => 'TransportationsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.transportations.edit', 'uses' => 'TransportationsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.transportations.delete', 'uses' => 'TransportationsController@delete']);
        });

             Route::group(['prefix' => 'reports'], function () {
                 
             Route::get('/print/{id}', ['as' => 'admin.reports.print', 'uses' => 'ReportsController@getEdit']);
                 
             Route::get('/absent', ['as' => 'admin.reports.absent', 'uses' => 'ReportsController@absent']);
             Route::get('/studentdatacourse', ['as' => 'admin.reports.studentdatacourse', 'uses' => 'ReportsController@studentdatacourse']);
             Route::get('/teacher', ['as' => 'admin.reports.teacher', 'uses' => 'ReportsController@teacherss']);
             Route::get('/students', ['as' => 'admin.reports.students', 'uses' => 'ReportsController@students']);
             Route::get('/Exemptstudent', ['as' => 'admin.reports.Exemptstudent', 'uses' => 'ReportsController@Exemptstudent']);
             Route::get('/levelss', ['as' => 'admin.reports.levelss', 'uses' => 'ReportsController@levels']);
             Route::get('/counts', ['as' => 'admin.reports.counts', 'uses' => 'ReportsController@counts']);
             Route::get('/Scounts', ['as' => 'admin.reports.Scounts', 'uses' => 'ReportsController@Scounts']);
             Route::get('/grades', ['as' => 'admin.reports.grades', 'uses' => 'ReportsController@grades']);
             Route::get('/attend', ['as' => 'admin.reports.attend', 'uses' => 'ReportsController@attend']);
             Route::get('/salaries', ['as' => 'admin.reports.salaries', 'uses' => 'ReportsController@salariesReport']);
             Route::get('/studentss', ['as' => 'admin.reports.studentss', 'uses' => 'ReportsController@studentss']);
             Route::get('/datacenter', ['as' => 'admin.reports.datacenter', 'uses' => 'ReportsController@datacenter']);
             Route::get('/studentcourse', ['as' => 'admin.reports.studentcourse', 'uses' => 'ReportsController@studentcourse']);
             Route::get('/levelstudents', ['as' => 'admin.reports.levelstudents', 'uses' => 'ReportsController@levelstudents']);
            Route::get('/parts', ['as' => 'admin.reports.parts', 'uses' => 'ReportsController@parts']);
            Route::get('/coursebookstudent', ['as' => 'admin.reports.coursebookstudent', 'uses' => 'ReportsController@coursebookstudent']);
            Route::get('/coursereservation', ['as' => 'admin.reports.coursereservation', 'uses' => 'ReportsController@coursereservation']);
            Route::get('/financialclaims', ['as' => 'admin.reports.financialclaims', 'uses' => 'ReportsController@financialclaims']);
            Route::get('/bookstudents', ['as' => 'admin.reports.bookstudents', 'uses' => 'ReportsController@bookstudents']);
            Route::get('/differentparties', ['as' => 'admin.reports.differentparties', 'uses' => 'ReportsController@differentparties']);
            Route::get('/passlevel', ['as' => 'admin.reports.passlevel', 'uses' => 'ReportsController@passlevel']);
            Route::get('/passlevels', ['as' => 'admin.reports.passlevels', 'uses' => 'ReportsController@passlevels']);
            Route::get('/finaltest', ['as' => 'admin.reports.finaltest', 'uses' => 'ReportsController@finaltest']);
            Route::get('/finaltests', ['as' => 'admin.reports.finaltests', 'uses' => 'ReportsController@finaltests']);
            Route::get('/graduation', ['as' => 'admin.reports.graduation', 'uses' => 'ReportsController@graduation']);
            Route::get('/graduations', ['as' => 'admin.reports.graduations', 'uses' => 'ReportsController@graduations']);
            Route::get('/skilldegree', ['as' => 'admin.reports.skilldegree', 'uses' => 'ReportsController@skilldegree']);
            Route::get('/passlevels', ['as' => 'admin.reports.passlevels', 'uses' => 'ReportsController@stats']);
             Route::get('/graduationcertificate', ['as' => 'admin.reports.graduationcertificate', 'uses' => 'ReportsController@graduationss']);
             Route::get('/prints/{id}', ['as' => 'admin.reports.prints', 'uses' => 'ReportsController@getprint']);
            Route::get('/printss/{id}', ['as' => 'admin.reports.printss', 'uses' => 'ReportsController@getprints']);
            Route::get('/printsss/{id}', ['as' => 'admin.reports.printsss', 'uses' => 'ReportsController@getprintss']);
       
            
           
        }); 

        Route::group(['prefix' => 'teachers'], function () {
            Route::get('/', ['as' => 'admin.teachers', 'uses' => 'TeachersController@getIndex']);
            Route::get('/add', ['as' => 'admin.teachers.add', 'uses' => 'TeachersController@getAdd']);
            Route::post('/add', ['as' => 'admin.teachers.add', 'uses' => 'TeachersController@insert']);
            Route::get('/files', ['as' => 'admin.teachers.files', 'uses' => 'TeachersController@getFile']);
            Route::get('/downloadFile/{id}',[ 'uses' => 'TeachersController@downloadFile']);
            Route::get('/edit/{id}', ['as' => 'admin.teachers.edit', 'uses' => 'TeachersController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.teachers.edit', 'uses' => 'TeachersController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.teachers.delete', 'uses' => 'TeachersController@delete']);
            Route::post('/addDoc', ['as' => 'admin.teacherDoc.add', 'uses' => 'TeachersController@addDoc']);
            Route::get('/deleteDoc/{id}', ['as' => 'admin.teacherDoc.delete', 'uses' => 'TeachersController@deleteDoc']);
            Route::get('/tdelete/{id}', ['as' => 'admin.tmaterial.delete', 'uses' => 'TeachersController@tdelete']);
            Route::get('/ldelete/{id}', ['as' => 'admin.tlevel.delete', 'uses' => 'TeachersController@tlevel']);
            Route::get('/attend', ['as' => 'admin.teachers.attend', 'uses' => 'TeachersController@attend']);
            Route::get('/leave', ['as' => 'admin.teachers.leave', 'uses' => 'TeachersController@leave']);
            Route::post('/attend', ['as' => 'admin.teachers.attend', 'uses' => 'TeachersController@postattend']);
            Route::post('/leave', ['as' => 'admin.teachers.leave', 'uses' => 'TeachersController@postleave']);
            Route::get('/jobs', ['as' => 'admin.jobs', 'uses' => 'TeachersController@jobs']);
            Route::get('/jobAdd', ['as' => 'admin.jobs.add', 'uses' => 'TeachersController@getAddJob']);
            Route::post('/jobAdd', ['as' => 'admin.jobs.add', 'uses' => 'TeachersController@addJob']);
            Route::get('/jobEdit/{id}', ['as' => 'admin.jobs.edit', 'uses' => 'TeachersController@jobEdit']);
            Route::post('/jobEdit/{id}', ['as' => 'admin.jobs.edit', 'uses' => 'TeachersController@postJobEdit']);
            Route::get('/jobDelete/{id}', ['as' => 'admin.jobs.delete', 'uses' => 'TeachersController@jobDelete']);
            Route::get('/time/{id}', ['as' => 'admin.teachers.time', 'uses' => 'TeachersController@time']);
            Route::post('/addtime', ['as' => 'admin.teachers.addtime', 'uses' => 'TeachersController@insertTime']);
            Route::get('/salary', ['as' => 'admin.salary.add', 'uses' => 'TeachersController@getSalary']);
            Route::post('/part', ['as' => 'admin.part.add', 'uses' => 'TeachersController@insertPart']);
            Route::get('/salaries/{id}', ['as' => 'admin.teachers.salaries', 'uses' => 'TeachersController@salaries']);
            Route::get('/code/{id}', ['as' => 'admin.teachers.code', 'uses' => 'TeachersController@code']);
        });

        Route::group(['prefix' => 'materials'], function () {
            Route::get('/', ['as' => 'admin.materials', 'uses' => 'MaterialsController@getIndex']);
            Route::get('/add', ['as' => 'admin.materials.add', 'uses' => 'MaterialsController@getAdd']);
            Route::post('/add', ['as' => 'admin.materials.add', 'uses' => 'MaterialsController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.materials.edit', 'uses' => 'MaterialsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.materials.edit', 'uses' => 'MaterialsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.materials.delete', 'uses' => 'MaterialsController@delete']);
            Route::post('/addDoc', ['as' => 'admin.materialDoc.add', 'uses' => 'MaterialsController@addDoc']);
            Route::get('/deleteDoc/{id}', ['as' => 'admin.materialDoc.delete', 'uses' => 'MaterialsController@deleteDoc']);
            Route::post('/addP', ['as' => 'admin.percent.add', 'uses' => 'MaterialsController@addPercent']);
            Route::get('/deleteP/{id}', ['as' => 'admin.percent.delete', 'uses' => 'MaterialsController@deletePercent']);
            Route::get('/cdelete/{id}', ['as' => 'admin.mcourse.delete', 'uses' => 'MaterialsController@cdelete']);
            Route::get('/tdelete/{id}', ['as' => 'admin.tcourse.delete', 'uses' => 'MaterialsController@tdelete']);
            Route::get('/files', ['as' => 'admin.materials.files', 'uses' => 'MaterialsController@files']);
            Route::get('/downloadFile/{id}',[ 'uses' => 'MaterialsController@downloadFile']);

            Route::get('/grades', ['as' => 'admin.materials.grades', 'uses' => 'MaterialsController@grades']);

            Route::get('/gradesedit/{id}', ['as' => 'admin.materials.gradesedit', 'uses' => 'MaterialsController@edit']);
        });

        Route::group(['prefix' => 'courses'], function () {
            Route::get('/', ['as' => 'admin.courses', 'uses' => 'CoursesController@getIndex']);
            Route::get('/add', ['as' => 'admin.courses.add', 'uses' => 'CoursesController@getAdd']);
            Route::post('/add', ['as' => 'admin.courses.add', 'uses' => 'CoursesController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.courses.edit', 'uses' => 'CoursesController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.courses.edit', 'uses' => 'CoursesController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.courses.delete', 'uses' => 'CoursesController@delete']);
            Route::get('/mdelete/{id}', ['as' => 'admin.coursematerial.delete', 'uses' => 'CoursesController@mdelete']);
            Route::get('/ldelete/{id}', ['as' => 'admin.tcourse.delete', 'uses' => 'CoursesController@tlevel']);
            Route::get('/coursetime/{id}', ['as' => 'admin.coursetime.delete', 'uses' => 'CoursesController@coursetime']);
        });

        Route::group(['prefix' => 'levels'], function () {
            Route::get('/', ['as' => 'admin.levels', 'uses' => 'LevelsController@getIndex']);
            Route::get('/add', ['as' => 'admin.levels.add', 'uses' => 'LevelsController@getAdd']);
            Route::post('/add', ['as' => 'admin.levels.add', 'uses' => 'LevelsController@insert']);
            Route::get('/edit/{id}', ['as' => 'admin.levels.edit', 'uses' => 'LevelsController@getEdit']);
            Route::post('/edit/{id}', ['as' => 'admin.levels.edit', 'uses' => 'LevelsController@postEdit']);
            Route::get('/delete/{id}', ['as' => 'admin.levels.delete', 'uses' => 'LevelsController@delete']);
            Route::get('/delete/{id}', ['as' => 'admin.levels.delete', 'uses' => 'LevelsController@delete']);
            Route::get('/getcoursedata', ['as' => 'ajaxdata.getcoursedata', 'uses' => 'LevelsController@getcoursedata']);
        });


        Route::group(['prefix' => 'subscribers'], function () {
            Route::get('/index', ['as' => 'admin.subscribers', 'uses' => 'SubscribersController@getIndex']);
            Route::get('/send/{id}', ['as' => 'admin.subscriber.send', 'uses' => 'SubscribersController@getEmail']);
            Route::post('/sendMail', ['as' => 'sendMail', 'uses' => 'SubscribersController@sendEmail']);
            Route::get('/sendAll', ['as' => 'admin.subscriber.sendAll', 'uses' => 'SubscribersController@getAll']);
            Route::post('/sendAll', ['as' => 'admin.subscriber.sendAll', 'uses' => 'SubscribersController@sendAll']);
        });

        Route::group(['prefix' => 'users'], function () {
            Route::get('/', ['as' => 'admin.users', 'uses' => 'UsersController@getIndex']);
            Route::get('/add', ['as' => 'admin.user.add', 'uses' => 'UsersController@getAdd']);
            Route::post('/add', ['as' => 'admin.user.add', 'uses' => 'UsersController@insertUser']);
            Route::get('/edit/{id}', ['as' => 'admin.user.edit', 'uses' => 'UsersController@getUser']);
            Route::post('/edit/{id}', ['as' => 'admin.user.edit', 'uses' => 'UsersController@updateUser']);
            Route::get('/delete/{id}', ['as' => 'admin.user.delete', 'uses' => 'UsersController@deleteU']);
            Route::post('/active', ['as' => 'admin.user.active', 'uses' => 'UsersController@postActive']);
            Route::post('/disActive', ['as' => 'admin.user.disActive', 'uses' => 'UsersController@postDisActive']);
            Route::post('/block', ['as' => 'admin.user.block', 'uses' => 'UsersController@postBlock']);



        });
        Route::get('/message', ['as' => 'admin.message', 'uses' => 'MessageController@getIndex']);
        Route::get('/profile', ['as' => 'admin.profile', 'uses' => 'UsersController@profile']);
        Route::get('/permission', ['as' => 'admin.permission', 'uses' => 'PermissionController@get']);
        Route::post('/permission/create', ['as' => 'admin.permission.create', 'uses' => 'PermissionController@insert']);
        Route::post('/profile', ['as' => 'admin.profile.edit', 'uses' => 'UsersController@editProfile']);
        Route::post('/profileimg', ['as' => 'admin.profile.image', 'uses' => 'UsersController@editProfileImage']);
        Route::post('/profilepass', ['as' => 'admin.profile.pass', 'uses' => 'UsersController@editProfilePass']);
        Route::get('/order', ['as' => 'admin.order', 'uses' => 'MessageController@order']);
        Route::post('/upload', ['as' => 'admin.upload.post', 'uses' => 'UploadController@getPost']);
        Route::post('/uploadIcon', ['as' => 'admin.upload.icon', 'uses' => 'UploadController@getPost2']);
        Route::post('/uploadImage', ['as' => 'admin.upload.image', 'uses' => 'UploadController@getPost3']);
        Route::post('/uploads', 'DataController@dropzoneStore')->name('admin.dropzoneStore');
        Route::post('/upload/images', ['as' => 'admin.upload.images', 'uses' => 'CatsController@getPostImages']);




    });
});