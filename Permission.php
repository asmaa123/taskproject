<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model 
{

    protected $table = 'permissions';
    public $timestamps = true;

    public function pages()
    {
        return $this->belongsToMany('App\Page');
    }

}