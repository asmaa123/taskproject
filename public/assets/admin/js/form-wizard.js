var FormWizard=function(){return{init:function(){
    if(!jQuery().bootstrapWizard){
        return;

    }function format(state){
        if(!state.id)
            return state.text;

        return"<img class='flag' src='../../assets/global/img/flags/"+state.id.toLowerCase()+".png'/>&nbsp;&nbsp;"+state.text;

    }
    $('#put_total').text($('#form_wizard_1').find('li').length);

    var form=$('#submit_form');
    var error=$('.alert-danger',form);

    var success=$('.alert-success',form);
    form.validate({doNotHideMessage:true,
                   errorElement:'span',
                   errorClass:'help-block help-block-error',
                   focusInvalid:false,
                   rules:{
                        consult_name:{required:true},
                        consultant_id:{required:true},
                        time:{required:true},
                        date:{required:true},
                        email:{required:true,email:true},
                        phone:{required:true},
                        name:{required:true},
                        message:{required:true},
                            address:{required:true},city:{required:true},
                            country:{required:true},card_name:{required:true},
                            card_number:{minlength:16,maxlength:16,required:true},
                            card_cvc:{digits:true,required:true,minlength:3,maxlength:4},
                            card_expiry_date:{required:true},
                            'payment[]':{required:true,minlength:1}},
                            messages:{'consultant_id':{required:"من فضلك اختر مستشار",
                            minlength:jQuery.validator.format("من فضلك اختر مستشار")}},
                            errorPlacement:function(error,element){
                                if(element.attr("name")=="gender"){
                                    error.insertAfter("#form_gender_error");
}else if(element.attr("name")=="payment[]"){error.insertAfter("#form_payment_error");
}else{error.insertAfter(element);
    }},invalidHandler:function(event,validator){success.hide();
        error.show();
},highlight:function(element){$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
},unhighlight:function(element){$(element).closest('.form-group').removeClass('has-error');
},success:function(label){if(label.attr("for")=="gender"||label.attr("for")=="payment[]"){label.closest('.form-group').removeClass('has-error').addClass('has-success');
    label.remove();
}else{label.addClass('valid').closest('.form-group').removeClass('has-error').addClass('has-success');
}},submitHandler:function(form){success.show();
        error.hide();
    form[0].submit();
}});
    var displayConfirm=function(){
        $('#tab10 .form-control-static',form).each(function(){
            var input=$('[name="'+$(this).attr("data-display")+'"]',form);
        if(input.is(":radio")){input=$('[name="'+$(this).attr("data-display")+'"]:checked',form);
}if(input.is(":text")||input.is("textarea")){
    $(this).html(input.val());
}else if(input.is("select")){
    $(this).html(input.find('option:selected').text());
}else if(input.is(":radio")&&input.is(":checked")){
    $(this).html(input.attr("data-title"));
}else if($(this).attr("data-display")=='payment[]'){var payment=[];
        $('[name="payment[]"]:checked',form).each(function(){payment.push($(this).attr('data-title'));
});
    $(this).html(payment.join("<br>"));
}});
}
var handleTitle=function(tab,navigation,index){var total=navigation.find('li').length;
var current=index+1;
$('.step-title',$('#form_wizard_1')).text('Step '+(index+1)+' of '+total);
$('#current_step').text((index+1));
jQuery('li',$('#form_wizard_1')).removeClass("done");
var li_list=navigation.find('li');
for(var i=0; i<index; i++){
    jQuery(li_list[i]).addClass("done");
}
if(current==1){
    $('#form_wizard_1').find('.button-previous').hide();
}else{$('#form_wizard_1').find('.button-previous').show();
}if(current>=total){$('#form_wizard_1').find('.button-next').hide();
$('#form_wizard_1').find('.button-submit').show();
displayConfirm();
}else{$('#form_wizard_1').find('.button-next').show();
$('#form_wizard_1').find('.button-submit').hide();
}}
$('#form_wizard_1').bootstrapWizard({'nextSelector':'.button-next','previousSelector':'.button-previous',onTabClick:function(tab,navigation,index,clickedIndex){return false;
    success.hide();
error.hide();
if(form.valid()==false){return false;
}handleTitle(tab,navigation,clickedIndex);
},onNext:function(tab,navigation,index){success.hide();
    error.hide();
if(form.valid()==false){
    return false;
}handleTitle(tab,navigation,index);
},onPrevious:function(tab,navigation,index){success.hide();
    error.hide();
handleTitle(tab,navigation,index);
},onTabShow:function(tab,navigation,index){var total=navigation.find('li').length;
var current=index+1;
var $percent=(current/total)*100;
$('#form_wizard_1').find('.progress-bar').css({width:$percent+'%'});
}}).next();
$('#form_wizard_1').find('.button-previous').hide();
$('#form_wizard_1 .button-submit').click(function(){
    //swal({title:"Thank you",type:"success",confirmButtonClass:"swal-confirm",confirmButtonText:"close!",titleClass:"swal-text",closeOnConfirm:true,allowOutsideClick:true},function(){});
}).hide();
}};
}();
jQuery(document).ready(function(){FormWizard.init();
});
