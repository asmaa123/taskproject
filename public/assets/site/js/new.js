/* Dynamic sub category select
 =================================*/
(function ($) {
    $('#main-category').on('change', function () {
        var selectedVal = parseInt($(this).val());
        $('.subcat_select').addClass('hidden');
        $('#subCat' + selectedVal).removeClass('hidden');
    });
    $('#sortBySelect').on('change', function () {
        $('#sortByForm').submit();
    });
})(jQuery);
//add button
$(document).ready(function () {
    "use strict";
    $("#add").click(function () {
        var inputItem = $("#fileInputsGroup").find('.fileInputItem').first();
        inputItem.clone().appendTo("#fileInputsGroup");
    });
    $('body').on('click', '.removeFileItem', function () {
        var parentItem = $(this).parents('.fileInputItem');
        if (!parentItem.is(':first-child')) {
            parentItem.remove();
        }
        return false;
    });
});


$(document).on('click', '.img-circle', function () {
    alert("test");
    get_elment("result").innerHTML ="";
    var btn = $(this);
    var uploadInp = btn.next('input[type=file]');

    uploadInp.change(function () {
        if (validateImgFile(this)) {
            btn.html('');
            previewURL(btn, this);
        }
    }).click();
});

function previewURL(btn, input) {

    if (input.files && input.files[0]) {

        // collecting the file source
        var file = input.files[0];

        // preview the image
        var reader = new FileReader();
        reader.onload = function (e) {
            var src = e.target.result;
            btn.attr('src', src);
        };
        reader.readAsDataURL(file);

    }
}


//validating the file

function validateImgFile(input) {
    if (input.files && input.files[0]) {

        // collecting the file source
        var file = input.files[0];

        // validating the image name
        if (file.name.length < 1) {
            get_elment("result").innerHTML="<p>يرجى تحميل ملفات  JPG,PNG,GIF,JPEG</p>";
            return false;
        }
        // validating the image size
        else if (file.size > 2000000) {
            get_elment("result").innerHTML="<p>يرجى اختيار ملف اقل من 20 MB</p>";
            return false;
        }
        // validating the image type
        else if (file.type != 'image/png' && file.type != 'image/jpg' && file.type != 'image/gif' && file.type != 'image/jpeg') {
            get_elment("result").innerHTML="<p>يرجى تحميل ملفات  JPG,PNG,GIF,JPEG</p>";
            return false;
        }

        get_elment("result").innerHTML = "<p>تم تحميل الملف بنجاح </p>";
        
        return true;
    }
}

