<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('image', 65535)->nullable();
			$table->text('guardian_name', 65535)->nullable();
			$table->integer('guardian_id')->nullable();
			$table->text('username', 65535)->nullable();
			$table->text('password', 65535)->nullable();
			$table->text('recover', 65535)->nullable();
			$table->text('email', 65535)->nullable();
			$table->text('phone', 65535)->nullable();
			$table->text('job', 65535)->nullable();
			$table->text('whatsapp', 65535)->nullable();
			$table->text('national_id', 65535)->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('nationality', 65535)->nullable();
			$table->text('remember_token', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}

}
