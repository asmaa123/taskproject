<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseTimesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('course_times', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('course_id')->nullable();
			$table->text('day', 65535)->nullable();
			$table->text('time_from', 65535)->nullable();
			$table->text('time_to', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('course_times');
	}

}
