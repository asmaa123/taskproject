<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePercentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('percents', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('percent_name', 65535)->nullable();
			$table->integer('grade')->nullable();
			$table->integer('material_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('percents');
	}

}
