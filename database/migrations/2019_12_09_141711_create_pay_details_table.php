<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePayDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pay_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('amount')->nullable();
			$table->date('date')->nullable();
			$table->integer('student_id')->nullable();
			$table->integer('pay_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pay_details');
	}

}
