<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentArchivesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_archives', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('center_id')->nullable();
			$table->integer('level_id')->nullable();
			$table->integer('season_id')->nullable();
			$table->integer('student_id')->nullable();
			$table->text('year', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_archives');
	}

}
