<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttendTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attend', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('teacher_id');
			$table->integer('student_id')->nullable();
			$table->integer('center_id')->nullable();
			$table->date('date')->nullable();
			$table->text('days', 65535)->nullable();
			$table->text('month', 65535)->nullable();
			$table->text('year', 65535)->nullable();
			$table->time('attends')->nullable();
			$table->time('leaves')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attend');
	}

}
