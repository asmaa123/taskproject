<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrgsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orgs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('logo', 65535)->nullable();
			$table->text('name', 65535)->nullable();
			$table->text('address', 65535)->nullable();
			$table->text('business_registration', 65535)->nullable();
			$table->text('tax_card', 65535)->nullable();
			$table->text('phone', 65535)->nullable();
			$table->text('fax', 65535)->nullable();
			$table->text('email', 65535)->nullable();
			$table->text('website', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orgs');
	}

}
