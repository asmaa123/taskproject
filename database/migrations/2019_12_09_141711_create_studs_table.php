<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('studs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('name', 65535)->nullable();
			$table->text('center', 65535)->nullable();
			$table->text('course', 65535)->nullable();
			$table->text('birth', 65535)->nullable();
			$table->text('national_id', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('studs');
	}

}
