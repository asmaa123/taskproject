<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('course_name', 65535)->nullable();
			$table->integer('max_num')->nullable();
			$table->integer('time')->nullable();
			$table->integer('center_id')->nullable();
			$table->integer('coursetype_id')->nullable();
			$table->integer('active')->nullable();
			$table->timestamps();
			$table->integer('type_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}
