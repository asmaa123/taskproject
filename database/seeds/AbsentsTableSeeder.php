<?php

use Illuminate\Database\Seeder;

class AbsentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('absents')->delete();
        
        \DB::table('absents')->insert(array (
            0 => 
            array (
                'id' => 25,
                'student_id' => 1,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-16',
                'status' => 0,
                'created_at' => '2019-11-16 19:32:22',
                'updated_at' => '2019-11-16 19:32:22',
            ),
            1 => 
            array (
                'id' => 24,
                'student_id' => NULL,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-16',
                'status' => 0,
                'created_at' => '2019-11-16 19:32:22',
                'updated_at' => '2019-11-16 19:32:22',
            ),
            2 => 
            array (
                'id' => 15,
                'student_id' => NULL,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-14',
                'status' => 0,
                'created_at' => '2019-11-14 07:43:37',
                'updated_at' => '2019-11-14 07:43:37',
            ),
            3 => 
            array (
                'id' => 14,
                'student_id' => NULL,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-14',
                'status' => 0,
                'created_at' => '2019-11-14 07:43:37',
                'updated_at' => '2019-11-14 07:43:37',
            ),
            4 => 
            array (
                'id' => 13,
                'student_id' => NULL,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-14',
                'status' => 0,
                'created_at' => '2019-11-14 07:43:37',
                'updated_at' => '2019-11-14 07:43:37',
            ),
            5 => 
            array (
                'id' => 12,
                'student_id' => 28,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-14',
                'status' => 0,
                'created_at' => '2019-11-14 06:41:09',
                'updated_at' => '2019-11-14 06:41:09',
            ),
            6 => 
            array (
                'id' => 11,
                'student_id' => 11,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-14',
                'status' => 1,
                'created_at' => '2019-11-14 06:41:09',
                'updated_at' => '2019-11-14 06:41:09',
            ),
            7 => 
            array (
                'id' => 10,
                'student_id' => 17,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-14',
                'status' => 1,
                'created_at' => '2019-11-14 06:41:09',
                'updated_at' => '2019-11-14 06:41:09',
            ),
            8 => 
            array (
                'id' => 9,
                'student_id' => 14,
                'center_id' => 5,
                'course_id' => 2,
                'date' => '2019-11-07',
                'status' => 0,
                'created_at' => '2019-11-07 14:08:30',
                'updated_at' => '2019-11-07 14:08:30',
            ),
            9 => 
            array (
                'id' => 8,
                'student_id' => 22,
                'center_id' => 5,
                'course_id' => 2,
                'date' => '2019-11-07',
                'status' => 0,
                'created_at' => '2019-11-07 14:08:30',
                'updated_at' => '2019-11-07 14:08:30',
            ),
            10 => 
            array (
                'id' => 7,
                'student_id' => 11,
                'center_id' => 5,
                'course_id' => 2,
                'date' => '2019-11-07',
                'status' => 0,
                'created_at' => '2019-11-07 14:08:30',
                'updated_at' => '2019-11-07 14:08:30',
            ),
            11 => 
            array (
                'id' => 6,
                'student_id' => 27,
                'center_id' => 5,
                'course_id' => 2,
                'date' => '2019-11-07',
                'status' => 0,
                'created_at' => '2019-11-07 14:08:29',
                'updated_at' => '2019-11-07 14:08:29',
            ),
            12 => 
            array (
                'id' => 38,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            13 => 
            array (
                'id' => 37,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            14 => 
            array (
                'id' => 36,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            15 => 
            array (
                'id' => 35,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            16 => 
            array (
                'id' => 34,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            17 => 
            array (
                'id' => 33,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            18 => 
            array (
                'id' => 32,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            19 => 
            array (
                'id' => 31,
                'student_id' => NULL,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-09',
                'status' => 0,
                'created_at' => '2019-12-09 11:02:08',
                'updated_at' => '2019-12-09 11:02:08',
            ),
            20 => 
            array (
                'id' => 5,
                'student_id' => 26,
                'center_id' => 5,
                'course_id' => 2,
                'date' => '2019-11-07',
                'status' => 1,
                'created_at' => '2019-11-07 14:08:29',
                'updated_at' => '2019-11-07 14:08:29',
            ),
            21 => 
            array (
                'id' => 4,
                'student_id' => 20,
                'center_id' => 5,
                'course_id' => 2,
                'date' => '2019-11-07',
                'status' => 1,
                'created_at' => '2019-11-07 14:08:29',
                'updated_at' => '2019-11-07 14:08:29',
            ),
            22 => 
            array (
                'id' => 29,
                'student_id' => 1,
                'center_id' => 1,
                'course_id' => 1,
                'date' => '2019-12-02',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:08:12',
            ),
            23 => 
            array (
                'id' => 30,
                'student_id' => 2,
                'center_id' => 1,
                'course_id' => 8,
                'date' => '2019-12-17',
                'status' => 0,
                'created_at' => '2019-12-04 14:26:14',
                'updated_at' => '2019-12-04 14:26:14',
            ),
            24 => 
            array (
                'id' => 26,
                'student_id' => 1,
                'center_id' => 3,
                'course_id' => 8,
                'date' => '2019-11-16',
                'status' => 0,
                'created_at' => '2019-11-16 19:32:22',
                'updated_at' => '2019-11-16 19:32:22',
            ),
        ));
        
        
    }
}