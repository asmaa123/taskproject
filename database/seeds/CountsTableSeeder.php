<?php

use Illuminate\Database\Seeder;

class CountsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('counts')->delete();
        
        \DB::table('counts')->insert(array (
            0 => 
            array (
                'id' => 56,
                'student_id' => 57,
                'amount' => -122,
                'discount' => NULL,
                'date' => '2019-12-09',
                'notes' => 'مصروفات الطالب yoyo',
                'created_at' => NULL,
                'updated_at' => '2019-12-09 03:57:16',
            ),
            1 => 
            array (
                'id' => 55,
                'student_id' => 56,
                'amount' => -12,
                'discount' => NULL,
                'date' => '2019-12-09',
                'notes' => 'مصروفات الطالب asmaa karam abozied2121',
                'created_at' => NULL,
                'updated_at' => '2019-12-09 03:33:31',
            ),
            2 => 
            array (
                'id' => 54,
                'student_id' => 55,
                'amount' => -11,
                'discount' => NULL,
                'date' => '2019-12-09',
                'notes' => 'مصروفات الطالب kokokoko',
                'created_at' => NULL,
                'updated_at' => '2019-12-09 03:22:40',
            ),
            3 => 
            array (
                'id' => 53,
                'student_id' => 54,
                'amount' => -12,
                'discount' => NULL,
                'date' => '2019-12-09',
                'notes' => 'مصروفات الطالب asmaa karam abozied',
                'created_at' => NULL,
                'updated_at' => '2019-12-09 03:19:41',
            ),
            4 => 
            array (
                'id' => 52,
                'student_id' => 53,
                'amount' => -40,
                'discount' => NULL,
                'date' => '2019-12-08',
                'notes' => 'مصروفات الطالب ww',
                'created_at' => NULL,
                'updated_at' => '2019-12-08 09:50:53',
            ),
            5 => 
            array (
                'id' => 51,
                'student_id' => 52,
                'amount' => -250,
                'discount' => NULL,
                'date' => '2019-12-08',
                'notes' => 'مصروفات الطالب asmaa karam',
                'created_at' => NULL,
                'updated_at' => '2019-12-08 09:49:55',
            ),
            6 => 
            array (
                'id' => 50,
                'student_id' => 51,
                'amount' => -2,
                'discount' => 2,
                'date' => '2019-12-05',
                'notes' => 'مصروفات الطالب nana',
                'created_at' => NULL,
                'updated_at' => '2019-12-05 06:04:07',
            ),
            7 => 
            array (
                'id' => 49,
                'student_id' => 50,
                'amount' => -12,
                'discount' => NULL,
                'date' => '2019-12-05',
                'notes' => 'مصروفات الطالب asm',
                'created_at' => NULL,
                'updated_at' => '2019-12-05 05:03:46',
            ),
            8 => 
            array (
                'id' => 47,
                'student_id' => 2,
                'amount' => -1,
                'discount' => 1,
                'date' => '2019-12-05',
                'notes' => 'مصروفات الطالب asmaa',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 07:59:18',
            ),
            9 => 
            array (
                'id' => 48,
                'student_id' => 49,
                'amount' => -11,
                'discount' => NULL,
                'date' => '2019-12-04',
                'notes' => 'مصروفات الطالب asmaa karam',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 08:04:11',
            ),
            10 => 
            array (
                'id' => 1,
                'student_id' => 1,
                'amount' => -80,
                'discount' => 20,
                'date' => '2019-12-03',
                'notes' => 'مصروفات الطالب asmaa',
                'created_at' => NULL,
                'updated_at' => '2019-12-03 09:46:39',
            ),
            11 => 
            array (
                'id' => 46,
                'student_id' => 47,
                'amount' => -11,
                'discount' => NULL,
                'date' => '2019-12-04',
                'notes' => 'مصروفات الطالب asmaa karam',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 07:17:50',
            ),
        ));
        
        
    }
}