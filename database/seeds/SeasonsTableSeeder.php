<?php

use Illuminate\Database\Seeder;

class SeasonsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('seasons')->delete();
        
        \DB::table('seasons')->insert(array (
            0 => 
            array (
                'id' => 38,
                'season_name' => 'الفصل العاشر1',
                'season_year' => '2019',
                'price' => 122,
                'coursetype_id' => 46,
                'active' => 1,
                'created_at' => '2019-12-09 09:38:25',
                'updated_at' => '2019-12-09 09:39:04',
            ),
            1 => 
            array (
                'id' => 37,
                'season_name' => 'فصل دراسي اول 8-12-2019',
                'season_year' => '2019-2020',
                'price' => 250,
                'coursetype_id' => 56,
                'active' => 1,
                'created_at' => '2019-12-08 07:51:21',
                'updated_at' => '2019-12-08 07:51:21',
            ),
            2 => 
            array (
                'id' => 35,
                'season_name' => 'الفصل السادس',
                'season_year' => '2019',
                'price' => 12,
                'coursetype_id' => 52,
                'active' => 1,
                'created_at' => '2019-12-05 09:26:09',
                'updated_at' => '2019-12-05 09:26:09',
            ),
            3 => 
            array (
                'id' => 34,
                'season_name' => 'الاول ٢٠٢٠',
                'season_year' => '2019/2020',
                'price' => 40,
                'coursetype_id' => 52,
                'active' => 1,
                'created_at' => '2019-12-04 18:24:41',
                'updated_at' => '2019-12-04 18:24:41',
            ),
            4 => 
            array (
                'id' => 33,
                'season_name' => 'الفصل الدراسي الخامس',
                'season_year' => '2019',
                'price' => 12,
                'coursetype_id' => 50,
                'active' => 1,
                'created_at' => '2019-12-04 14:09:08',
                'updated_at' => '2019-12-04 14:09:08',
            ),
            5 => 
            array (
                'id' => 1,
                'season_name' => 'الاول',
                'season_year' => '2019/2020',
                'price' => 40,
                'coursetype_id' => 1,
                'active' => 1,
                'created_at' => '2019-12-03 15:19:42',
                'updated_at' => '2019-12-03 15:19:42',
            ),
            6 => 
            array (
                'id' => 32,
                'season_name' => 'asmaa karam a',
                'season_year' => '2019',
                'price' => 12,
                'coursetype_id' => 47,
                'active' => 1,
                'created_at' => '2019-12-04 13:06:37',
                'updated_at' => '2019-12-04 13:06:37',
            ),
            7 => 
            array (
                'id' => 29,
                'season_name' => 'فصل الاول1',
                'season_year' => '2019',
                'price' => 11,
                'coursetype_id' => 2,
                'active' => 1,
                'created_at' => '2019-12-03 14:25:56',
                'updated_at' => '2019-12-03 14:26:16',
            ),
            8 => 
            array (
                'id' => 36,
                'season_name' => 'الاول-ارتقاء ٢٠٢٠',
                'season_year' => '2019/2020',
                'price' => 40,
                'coursetype_id' => 55,
                'active' => 1,
                'created_at' => '2019-12-05 18:36:12',
                'updated_at' => '2019-12-05 18:36:12',
            ),
        ));
        
        
    }
}