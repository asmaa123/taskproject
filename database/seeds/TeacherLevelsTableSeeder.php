<?php

use Illuminate\Database\Seeder;

class TeacherLevelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teacher_levels')->delete();
        
        \DB::table('teacher_levels')->insert(array (
            0 => 
            array (
                'id' => 8,
                'teacher_id' => 30,
                'level_id' => 3,
                'created_at' => '2019-12-08 15:34:54',
                'updated_at' => '2019-12-08 15:34:54',
            ),
            1 => 
            array (
                'id' => 7,
                'teacher_id' => 29,
                'level_id' => 3,
                'created_at' => '2019-12-08 12:49:58',
                'updated_at' => '2019-12-08 12:49:58',
            ),
            2 => 
            array (
                'id' => 6,
                'teacher_id' => 24,
                'level_id' => 2,
                'created_at' => '2019-12-04 14:24:13',
                'updated_at' => '2019-12-04 14:24:13',
            ),
            3 => 
            array (
                'id' => 1,
                'teacher_id' => 1,
                'level_id' => 1,
                'created_at' => '2019-12-04 09:25:53',
                'updated_at' => '2019-12-04 09:25:53',
            ),
            4 => 
            array (
                'id' => 9,
                'teacher_id' => 30,
                'level_id' => 53,
                'created_at' => '2019-12-08 15:37:04',
                'updated_at' => '2019-12-08 15:37:04',
            ),
            5 => 
            array (
                'id' => 10,
                'teacher_id' => 29,
                'level_id' => 53,
                'created_at' => '2019-12-08 15:40:08',
                'updated_at' => '2019-12-08 15:40:08',
            ),
            6 => 
            array (
                'id' => 11,
                'teacher_id' => 30,
                'level_id' => 49,
                'created_at' => '2019-12-09 07:18:36',
                'updated_at' => '2019-12-09 07:18:36',
            ),
            7 => 
            array (
                'id' => 12,
                'teacher_id' => 31,
                'level_id' => 53,
                'created_at' => '2019-12-09 10:55:59',
                'updated_at' => '2019-12-09 10:55:59',
            ),
        ));
        
        
    }
}