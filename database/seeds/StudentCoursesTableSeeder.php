<?php

use Illuminate\Database\Seeder;

class StudentCoursesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student_courses')->delete();
        
        \DB::table('student_courses')->insert(array (
            0 => 
            array (
                'id' => 122,
                'student_id' => 53,
                'course_id' => 35,
                'created_at' => '2019-12-09 10:54:31',
                'updated_at' => '2019-12-09 10:54:31',
            ),
            1 => 
            array (
                'id' => 121,
                'student_id' => 57,
                'course_id' => 1,
                'created_at' => '2019-12-09 09:57:16',
                'updated_at' => '2019-12-09 09:57:16',
            ),
            2 => 
            array (
                'id' => 120,
                'student_id' => 56,
                'course_id' => 1,
                'created_at' => '2019-12-09 09:33:31',
                'updated_at' => '2019-12-09 09:33:31',
            ),
            3 => 
            array (
                'id' => 110,
                'student_id' => 48,
                'course_id' => 11,
                'created_at' => '2019-12-08 14:58:44',
                'updated_at' => '2019-12-08 14:58:44',
            ),
            4 => 
            array (
                'id' => 113,
                'student_id' => 50,
                'course_id' => 34,
                'created_at' => '2019-12-08 15:29:45',
                'updated_at' => '2019-12-08 15:29:45',
            ),
            5 => 
            array (
                'id' => 109,
                'student_id' => 51,
                'course_id' => 11,
                'created_at' => '2019-12-08 14:52:28',
                'updated_at' => '2019-12-08 14:52:28',
            ),
            6 => 
            array (
                'id' => 118,
                'student_id' => 54,
                'course_id' => 1,
                'created_at' => '2019-12-09 09:19:41',
                'updated_at' => '2019-12-09 09:19:41',
            ),
            7 => 
            array (
                'id' => 119,
                'student_id' => 55,
                'course_id' => 1,
                'created_at' => '2019-12-09 09:22:40',
                'updated_at' => '2019-12-09 09:22:40',
            ),
            8 => 
            array (
                'id' => 112,
                'student_id' => 2,
                'course_id' => 11,
                'created_at' => '2019-12-08 15:26:03',
                'updated_at' => '2019-12-08 15:26:03',
            ),
            9 => 
            array (
                'id' => 108,
                'student_id' => 48,
                'course_id' => 2,
                'created_at' => '2019-12-08 13:09:49',
                'updated_at' => '2019-12-08 13:09:49',
            ),
            10 => 
            array (
                'id' => 111,
                'student_id' => 50,
                'course_id' => 11,
                'created_at' => '2019-12-08 15:21:01',
                'updated_at' => '2019-12-08 15:21:01',
            ),
            11 => 
            array (
                'id' => 117,
                'student_id' => 53,
                'course_id' => NULL,
                'created_at' => '2019-12-08 15:50:53',
                'updated_at' => '2019-12-08 15:50:53',
            ),
            12 => 
            array (
                'id' => 116,
                'student_id' => 52,
                'course_id' => NULL,
                'created_at' => '2019-12-08 15:49:55',
                'updated_at' => '2019-12-08 15:49:55',
            ),
            13 => 
            array (
                'id' => 114,
                'student_id' => 51,
                'course_id' => 34,
                'created_at' => '2019-12-08 15:30:24',
                'updated_at' => '2019-12-08 15:30:24',
            ),
            14 => 
            array (
                'id' => 107,
                'student_id' => 2,
                'course_id' => 2,
                'created_at' => '2019-12-08 12:52:03',
                'updated_at' => '2019-12-08 12:52:03',
            ),
            15 => 
            array (
                'id' => 106,
                'student_id' => 51,
                'course_id' => 2,
                'created_at' => '2019-12-08 12:50:18',
                'updated_at' => '2019-12-08 12:50:18',
            ),
            16 => 
            array (
                'id' => 105,
                'student_id' => 51,
                'course_id' => 8,
                'created_at' => '2019-12-08 12:49:31',
                'updated_at' => '2019-12-08 12:49:31',
            ),
            17 => 
            array (
                'id' => 104,
                'student_id' => 50,
                'course_id' => 8,
                'created_at' => '2019-12-08 12:49:08',
                'updated_at' => '2019-12-08 12:49:08',
            ),
            18 => 
            array (
                'id' => 103,
                'student_id' => 51,
                'course_id' => 1,
                'created_at' => '2019-12-05 12:04:07',
                'updated_at' => '2019-12-05 12:04:07',
            ),
            19 => 
            array (
                'id' => 102,
                'student_id' => 50,
                'course_id' => 1,
                'created_at' => '2019-12-05 11:03:46',
                'updated_at' => '2019-12-05 11:03:46',
            ),
            20 => 
            array (
                'id' => 101,
                'student_id' => 48,
                'course_id' => 1,
                'created_at' => '2019-12-05 10:52:02',
                'updated_at' => '2019-12-05 10:52:02',
            ),
            21 => 
            array (
                'id' => 100,
                'student_id' => 48,
                'course_id' => 8,
                'created_at' => '2019-12-04 14:23:54',
                'updated_at' => '2019-12-04 14:23:54',
            ),
            22 => 
            array (
                'id' => 115,
                'student_id' => 50,
                'course_id' => 36,
                'created_at' => '2019-12-08 15:34:01',
                'updated_at' => '2019-12-08 15:34:01',
            ),
            23 => 
            array (
                'id' => 99,
                'student_id' => 49,
                'course_id' => 1,
                'created_at' => '2019-12-04 14:04:11',
                'updated_at' => '2019-12-04 14:04:11',
            ),
            24 => 
            array (
                'id' => 98,
                'student_id' => 48,
                'course_id' => 4,
                'created_at' => '2019-12-04 13:59:18',
                'updated_at' => '2019-12-04 13:59:18',
            ),
            25 => 
            array (
                'id' => 97,
                'student_id' => 47,
                'course_id' => 2,
                'created_at' => '2019-12-04 13:17:50',
                'updated_at' => '2019-12-04 13:17:50',
            ),
            26 => 
            array (
                'id' => 1,
                'student_id' => 1,
                'course_id' => 1,
                'created_at' => '2019-12-04 09:25:25',
                'updated_at' => '2019-12-04 09:25:25',
            ),
            27 => 
            array (
                'id' => 2,
                'student_id' => 1,
                'course_id' => 2,
                'created_at' => '2019-12-03 15:47:20',
                'updated_at' => '2019-12-03 15:47:20',
            ),
            28 => 
            array (
                'id' => 94,
                'student_id' => 46,
                'course_id' => NULL,
                'created_at' => '2019-12-03 15:46:39',
                'updated_at' => '2019-12-03 15:46:39',
            ),
        ));
        
        
    }
}