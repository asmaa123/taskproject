<?php

use Illuminate\Database\Seeder;

class TransportationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transportations')->delete();
        
        \DB::table('transportations')->insert(array (
            0 => 
            array (
                'id' => 10,
                'sat' => 1,
                'sun' => NULL,
                'mon' => NULL,
                'tue' => NULL,
                'wed' => NULL,
                'thu' => NULL,
                'fri' => NULL,
                'arrival' => 'الشهداء3',
                'launch' => '11',
                'price' => 11,
                'bus' => '11',
                'manager_id' => 30,
                'center_id' => 53,
                'active' => 1,
                'created_at' => '2019-12-09 09:42:30',
                'updated_at' => '2019-12-09 09:42:30',
            ),
            1 => 
            array (
                'id' => 8,
                'sat' => 1,
                'sun' => NULL,
                'mon' => NULL,
                'tue' => 1,
                'wed' => 1,
                'thu' => NULL,
                'fri' => NULL,
                'arrival' => 'الشهداء225',
                'launch' => '11',
                'price' => 11,
                'bus' => '11',
                'manager_id' => 1,
                'center_id' => 39,
                'active' => 1,
                'created_at' => '2019-12-05 09:31:33',
                'updated_at' => '2019-12-09 09:43:09',
            ),
            2 => 
            array (
                'id' => 7,
                'sat' => NULL,
                'sun' => NULL,
                'mon' => NULL,
                'tue' => NULL,
                'wed' => NULL,
                'thu' => NULL,
                'fri' => NULL,
                'arrival' => '12',
                'launch' => '32',
                'price' => 21,
                'bus' => '22',
                'manager_id' => 1,
                'center_id' => 1,
                'active' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:26:50',
            ),
        ));
        
        
    }
}