<?php

use Illuminate\Database\Seeder;

class MaterialDocumentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('material_documents')->delete();
        
        \DB::table('material_documents')->insert(array (
            0 => 
            array (
                'id' => 55,
                'material_id' => 31,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'جامعة دمشق',
                'created_at' => '2019-12-08 09:40:06',
                'updated_at' => '2019-12-08 09:40:06',
            ),
            1 => 
            array (
                'id' => 44,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => 'qq',
                'created_at' => '2019-12-02 09:50:20',
                'updated_at' => '2019-12-02 09:50:20',
            ),
            2 => 
            array (
                'id' => 51,
                'material_id' => 1,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'asmaa',
                'created_at' => '2019-12-08 09:07:52',
                'updated_at' => '2019-12-08 09:07:52',
            ),
            3 => 
            array (
                'id' => 50,
                'material_id' => 1,
                'file' => 'psd-covers.jpg',
                'details' => 'درس 1 08-12-2019',
                'created_at' => '2019-12-08 07:42:09',
                'updated_at' => '2019-12-08 07:42:09',
            ),
            4 => 
            array (
                'id' => 52,
                'material_id' => 1,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'this is a image',
                'created_at' => '2019-12-08 09:09:20',
                'updated_at' => '2019-12-08 09:09:20',
            ),
            5 => 
            array (
                'id' => 53,
                'material_id' => 1,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'cf',
                'created_at' => '2019-12-08 09:32:17',
                'updated_at' => '2019-12-08 09:32:17',
            ),
            6 => 
            array (
                'id' => 34,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => 'asmaa',
                'created_at' => '2019-12-02 09:13:36',
                'updated_at' => '2019-12-02 09:13:36',
            ),
            7 => 
            array (
                'id' => 40,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => 'qqq',
                'created_at' => '2019-12-02 09:36:27',
                'updated_at' => '2019-12-02 09:36:27',
            ),
            8 => 
            array (
                'id' => 39,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => 'qqq',
                'created_at' => '2019-12-02 09:36:23',
                'updated_at' => '2019-12-02 09:36:23',
            ),
            9 => 
            array (
                'id' => 38,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => 'asmaa',
                'created_at' => '2019-12-02 09:35:38',
                'updated_at' => '2019-12-02 09:35:38',
            ),
            10 => 
            array (
                'id' => 41,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => 'rrr',
                'created_at' => '2019-12-02 09:38:51',
                'updated_at' => '2019-12-02 09:38:51',
            ),
            11 => 
            array (
                'id' => 42,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => '4',
                'created_at' => '2019-12-02 09:48:15',
                'updated_at' => '2019-12-02 09:48:15',
            ),
            12 => 
            array (
                'id' => 35,
                'material_id' => 1,
                'file' => '78587098_2475677739367296_4027285031234830336_o.jpg',
                'details' => 'aa',
                'created_at' => '2019-12-02 09:17:09',
                'updated_at' => '2019-12-02 09:17:09',
            ),
            13 => 
            array (
                'id' => 15,
                'material_id' => 3,
                'file' => 'img289.jpg',
                'details' => '',
                'created_at' => '2019-11-06 14:29:43',
                'updated_at' => '2019-11-06 14:29:43',
            ),
            14 => 
            array (
                'id' => 14,
                'material_id' => 3,
                'file' => 'img289.jpg',
                'details' => '',
                'created_at' => '2019-11-06 14:29:43',
                'updated_at' => '2019-11-06 14:29:43',
            ),
            15 => 
            array (
                'id' => 13,
                'material_id' => 3,
                'file' => 'img289.jpg',
                'details' => '',
                'created_at' => '2019-11-06 14:29:43',
                'updated_at' => '2019-11-06 14:29:43',
            ),
            16 => 
            array (
                'id' => 12,
                'material_id' => 3,
                'file' => 'img289.jpg',
                'details' => '',
                'created_at' => '2019-11-06 14:29:42',
                'updated_at' => '2019-11-06 14:29:42',
            ),
            17 => 
            array (
                'id' => 10,
                'material_id' => 3,
                'file' => 'img289.jpg',
                'details' => '',
                'created_at' => '2019-11-06 14:29:42',
                'updated_at' => '2019-11-06 14:29:42',
            ),
            18 => 
            array (
                'id' => 9,
                'material_id' => 3,
                'file' => 'img289.jpg',
                'details' => '',
                'created_at' => '2019-11-06 14:29:41',
                'updated_at' => '2019-11-06 14:29:41',
            ),
            19 => 
            array (
                'id' => 7,
                'material_id' => 2,
                'file' => 'asmaaabozied.pdf',
                'details' => '',
                'created_at' => '2019-11-06 14:28:52',
                'updated_at' => '2019-11-06 14:28:52',
            ),
            20 => 
            array (
                'id' => 17,
                'material_id' => 3,
                'file' => 'asmaaabozied.pdf',
                'details' => '',
                'created_at' => '2019-11-12 14:02:03',
                'updated_at' => '2019-11-12 14:02:03',
            ),
            21 => 
            array (
                'id' => 3,
                'material_id' => 2,
                'file' => 'img289.jpg',
                'details' => '12',
                'created_at' => '2019-11-06 14:12:42',
                'updated_at' => '2019-11-06 14:12:42',
            ),
            22 => 
            array (
                'id' => 2,
                'material_id' => 4,
                'file' => 'img289.jpg',
                'details' => '',
                'created_at' => '2019-11-06 10:35:07',
                'updated_at' => '2019-11-06 10:35:07',
            ),
            23 => 
            array (
                'id' => 5,
                'material_id' => 2,
                'file' => 'img289.jpg',
                'details' => 'assdef',
                'created_at' => '2019-11-06 14:25:54',
                'updated_at' => '2019-11-06 14:25:54',
            ),
            24 => 
            array (
                'id' => 46,
                'material_id' => 1,
                'file' => 'laimannung-GLrm3MJXxcI-unsplash.jpg',
                'details' => 'شسسسص',
                'created_at' => '2019-12-03 14:46:37',
                'updated_at' => '2019-12-03 14:46:37',
            ),
            25 => 
            array (
                'id' => 54,
                'material_id' => 31,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'cf',
                'created_at' => '2019-12-08 09:32:17',
                'updated_at' => '2019-12-08 09:32:17',
            ),
            26 => 
            array (
                'id' => 47,
                'material_id' => 1,
                'file' => 'img289.jpg',
                'details' => 'asmaa',
                'created_at' => '2019-12-04 14:31:54',
                'updated_at' => '2019-12-04 14:31:54',
            ),
            27 => 
            array (
                'id' => 56,
                'material_id' => 28,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'asmaa',
                'created_at' => '2019-12-08 09:45:35',
                'updated_at' => '2019-12-08 09:45:35',
            ),
            28 => 
            array (
                'id' => 57,
                'material_id' => 29,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'this is a image',
                'created_at' => '2019-12-09 09:48:45',
                'updated_at' => '2019-12-09 09:48:45',
            ),
            29 => 
            array (
                'id' => 58,
                'material_id' => 29,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => 'this is a image',
                'created_at' => '2019-12-09 09:48:45',
                'updated_at' => '2019-12-09 09:48:45',
            ),
            30 => 
            array (
                'id' => 59,
                'material_id' => NULL,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'details' => '7',
                'created_at' => '2019-12-09 09:49:28',
                'updated_at' => '2019-12-09 09:49:28',
            ),
        ));
        
        
    }
}