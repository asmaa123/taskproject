<?php

use Illuminate\Database\Seeder;

class SalariesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('salaries')->delete();
        
        \DB::table('salaries')->insert(array (
            0 => 
            array (
                'id' => 16,
                'teacher_id' => 30,
                'salary' => 200,
                'days' => 0,
                'hours' => '0',
                'month' => 'January',
                'year' => '2019/2020',
                'bonus' => 0,
                'minus' => 0,
                'parts' => 0,
                'final' => 200,
                'status' => 1,
                'notes' => 'مرتب شهر January',
                'created_at' => '2019-12-09 11:06:14',
                'updated_at' => '2019-12-09 11:06:14',
            ),
            1 => 
            array (
                'id' => 15,
                'teacher_id' => 29,
                'salary' => 43,
                'days' => 0,
                'hours' => '0',
                'month' => 'January',
                'year' => '2020/2021',
                'bonus' => 0,
                'minus' => 0,
                'parts' => 0,
                'final' => 43,
                'status' => 1,
                'notes' => 'مرتب شهر January',
                'created_at' => '2019-12-08 15:44:54',
                'updated_at' => '2019-12-08 15:44:54',
            ),
            2 => 
            array (
                'id' => 14,
                'teacher_id' => 1,
                'salary' => 423,
                'days' => 0,
                'hours' => '0',
                'month' => 'January',
                'year' => '2019/2020',
                'bonus' => 0,
                'minus' => 0,
                'parts' => 0,
                'final' => 423,
                'status' => 1,
                'notes' => 'مرتب شهر January',
                'created_at' => '2019-12-04 15:15:42',
                'updated_at' => '2019-12-04 15:15:42',
            ),
            3 => 
            array (
                'id' => 13,
                'teacher_id' => 1,
                'salary' => 100,
                'days' => 22,
                'hours' => '23',
                'month' => '12',
                'year' => '2016',
                'bonus' => 12,
                'minus' => 11,
                'parts' => 4333,
                'final' => 34,
                'status' => 1,
                'notes' => 'this is a note',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:16:53',
            ),
        ));
        
        
    }
}