<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payments')->delete();
        
        \DB::table('payments')->insert(array (
            0 => 
            array (
                'id' => 10,
                'student_id' => 9,
                'amount' => 90,
                'remain' => 10,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:52:58',
            ),
            1 => 
            array (
                'id' => 9,
                'student_id' => 8,
                'amount' => 80,
                'remain' => 10,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:52:07',
            ),
            2 => 
            array (
                'id' => 8,
                'student_id' => 7,
                'amount' => 70,
                'remain' => 10,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:52:07',
            ),
            3 => 
            array (
                'id' => 7,
                'student_id' => 6,
                'amount' => 62,
                'remain' => 30,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:51:42',
            ),
            4 => 
            array (
                'id' => 6,
                'student_id' => 5,
                'amount' => 41,
                'remain' => 41,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:51:42',
            ),
            5 => 
            array (
                'id' => 5,
                'student_id' => 4,
                'amount' => 40,
                'remain' => 41,
                'notes' => 'oipo;o',
                'code' => 'pl;[op',
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:51:06',
            ),
            6 => 
            array (
                'id' => 4,
                'student_id' => 3,
                'amount' => 30,
                'remain' => 10,
                'notes' => 'mnb mbm',
                'code' => 'nhjmk,hjl.',
                'type' => 'jhklkjl',
                'date' => 'ghjkl',
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:51:06',
            ),
            7 => 
            array (
                'id' => 2,
                'student_id' => 1,
                'amount' => 20,
                'remain' => NULL,
                'notes' => ';klk[l',
                'code' => 'lkp;op[',
                'type' => 'p;\'po[',
                'date' => 'pll',
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:50:36',
            ),
            8 => 
            array (
                'id' => 3,
                'student_id' => 2,
                'amount' => 22,
                'remain' => 10,
                'notes' => ';lpkljk',
                'code' => 'vfghgy',
                'type' => 'dfvghgfuj',
                'date' => 'fghjfki',
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:50:36',
            ),
            9 => 
            array (
                'id' => 1,
                'student_id' => 28,
                'amount' => 12,
                'remain' => 10,
                'notes' => 'هدا الدفع متاح لكل الاجهزه ',
                'code' => '1234',
                'type' => 'number',
                'date' => '1',
                'created_at' => '2019-11-05 06:33:20',
                'updated_at' => '2019-11-05 06:33:20',
            ),
            10 => 
            array (
                'id' => 16,
                'student_id' => 1,
                'amount' => 20,
                'remain' => 10,
                'notes' => 'حسابات الطلاب ',
                'code' => '12454',
                'type' => 'type1',
                'date' => '22-10-20189',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:40:04',
            ),
            11 => 
            array (
                'id' => 11,
                'student_id' => 10,
                'amount' => 100,
                'remain' => 1,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:52:58',
            ),
            12 => 
            array (
                'id' => 12,
                'student_id' => 11,
                'amount' => 111,
                'remain' => 11,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:53:17',
            ),
            13 => 
            array (
                'id' => 13,
                'student_id' => 12,
                'amount' => 112,
                'remain' => 123,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:53:17',
            ),
            14 => 
            array (
                'id' => 14,
                'student_id' => 13,
                'amount' => 13,
                'remain' => 1,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:53:40',
            ),
            15 => 
            array (
                'id' => 15,
                'student_id' => 14,
                'amount' => 410,
                'remain' => 10,
                'notes' => NULL,
                'code' => NULL,
                'type' => NULL,
                'date' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-11-28 08:53:40',
            ),
            16 => 
            array (
                'id' => 17,
                'student_id' => 48,
                'amount' => 9,
                'remain' => 0,
                'notes' => 'lk',
                'code' => '254',
                'type' => 'manual',
                'date' => '2019-12-05 08:26:02',
                'created_at' => '2019-12-05 08:26:02',
                'updated_at' => '2019-12-05 08:52:09',
            ),
            17 => 
            array (
                'id' => 18,
                'student_id' => 48,
                'amount' => 11,
                'remain' => 10,
                'notes' => 'this is  note',
                'code' => '111',
                'type' => 'manual',
                'date' => '2019-12-05 08:52:09',
                'created_at' => '2019-12-05 08:52:09',
                'updated_at' => '2019-12-05 08:52:09',
            ),
            18 => 
            array (
                'id' => 19,
                'student_id' => 51,
                'amount' => 8,
                'remain' => -2,
                'notes' => 'mjki',
                'code' => '1454',
                'type' => 'manual',
                'date' => '2019-12-09 11:05:03',
                'created_at' => '2019-12-09 11:05:03',
                'updated_at' => '2019-12-09 11:05:03',
            ),
        ));
        
        
    }
}