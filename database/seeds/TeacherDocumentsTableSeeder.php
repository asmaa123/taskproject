<?php

use Illuminate\Database\Seeder;

class TeacherDocumentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teacher_documents')->delete();
        
        \DB::table('teacher_documents')->insert(array (
            0 => 
            array (
                'id' => 17,
                'teacher_id' => 1,
            'file' => 'img289 (1) (1).jpg',
                'created_at' => '2019-12-05 10:06:22',
                'updated_at' => '2019-12-05 10:06:22',
            ),
            1 => 
            array (
                'id' => 18,
                'teacher_id' => 30,
            'file' => 'img289 (1) (1) (2) (1).jpg',
                'created_at' => '2019-12-09 09:52:50',
                'updated_at' => '2019-12-09 09:52:50',
            ),
        ));
        
        
    }
}