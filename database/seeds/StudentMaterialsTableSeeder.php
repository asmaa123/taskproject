<?php

use Illuminate\Database\Seeder;

class StudentMaterialsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('student_materials')->delete();
        
        \DB::table('student_materials')->insert(array (
            0 => 
            array (
                'id' => 11,
                'student_id' => 1,
                'material_id' => 1,
                'status' => 1,
                'notes' => 'this is a note',
                'created_at' => NULL,
                'updated_at' => '2019-12-04 04:24:02',
            ),
        ));
        
        
    }
}