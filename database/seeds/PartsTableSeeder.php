<?php

use Illuminate\Database\Seeder;

class PartsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('parts')->delete();
        
        \DB::table('parts')->insert(array (
            0 => 
            array (
                'id' => 7,
                'teacher_id' => 25,
                'material_id' => NULL,
                'final' => NULL,
                'total' => NULL,
                'percent' => NULL,
                'month' => 'February',
                'year' => '2019',
                'day' => 2,
                'part' => 10,
                'created_at' => '2019-12-05 10:44:20',
                'updated_at' => '2019-12-05 10:44:20',
            ),
            1 => 
            array (
                'id' => 6,
                'teacher_id' => 1,
                'material_id' => 1,
                'final' => '21',
                'total' => '100',
                'percent' => '15',
                'month' => 'January',
                'year' => '2018',
                'day' => 1,
                'part' => 20,
                'created_at' => '2019-12-04 14:34:50',
                'updated_at' => '2019-12-04 14:34:50',
            ),
            2 => 
            array (
                'id' => 1,
                'teacher_id' => 2,
                'material_id' => 2,
                'final' => '4',
                'total' => '32',
                'percent' => '10%',
                'month' => '12',
                'year' => '2020',
                'day' => 55,
                'part' => 80,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:03:50',
            ),
            3 => 
            array (
                'id' => 2,
                'teacher_id' => 1,
                'material_id' => 1,
                'final' => '21',
                'total' => '32',
                'percent' => '10%',
                'month' => '1-2-2014',
                'year' => '2016',
                'day' => 12,
                'part' => 155,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:03:50',
            ),
            4 => 
            array (
                'id' => 8,
                'teacher_id' => 31,
                'material_id' => NULL,
                'final' => NULL,
                'total' => NULL,
                'percent' => NULL,
                'month' => 'February',
                'year' => '2019',
                'day' => 1,
                'part' => 100,
                'created_at' => '2019-12-09 09:55:22',
                'updated_at' => '2019-12-09 09:55:22',
            ),
        ));
        
        
    }
}