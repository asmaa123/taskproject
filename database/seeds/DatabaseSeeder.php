<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AbsentsTableSeeder::class);
        $this->call(AttendTableSeeder::class);
        $this->call(CenterDocumentsTableSeeder::class);
        $this->call(CentersTableSeeder::class);
        $this->call(CountsTableSeeder::class);
        $this->call(CourseLevelsTableSeeder::class);
        $this->call(CourseMaterialsTableSeeder::class);
        $this->call(CourseTimesTableSeeder::class);
        $this->call(CourseTypesTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(DocsTableSeeder::class);
        $this->call(ExemptionsTableSeeder::class);
        $this->call(GuardiansTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(LevelMaterialsTableSeeder::class);
        $this->call(LevelsTableSeeder::class);
        $this->call(MaterialDocumentsTableSeeder::class);
        $this->call(MaterialsTableSeeder::class);
        $this->call(MembersTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(OrgsTableSeeder::class);
        $this->call(PartsTableSeeder::class);
        $this->call(PayDetailsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(PaymsTableSeeder::class);
        $this->call(PercentsTableSeeder::class);
        $this->call(SalariesTableSeeder::class);
        $this->call(SeasonsTableSeeder::class);
        $this->call(StudentArchivesTableSeeder::class);
        $this->call(StudentCoursesTableSeeder::class);
        $this->call(StudentDocumentsTableSeeder::class);
        $this->call(StudentGradesTableSeeder::class);
        $this->call(StudentLevelsTableSeeder::class);
        $this->call(StudentMaterialsTableSeeder::class);
        $this->call(StudentPercentsTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(StudsTableSeeder::class);
        $this->call(TeacherCoursesTableSeeder::class);
        $this->call(TeacherDocumentsTableSeeder::class);
        $this->call(TeacherLevelsTableSeeder::class);
        $this->call(TeacherMaterialsTableSeeder::class);
        $this->call(TeachersTableSeeder::class);
        $this->call(TimesTableSeeder::class);
        $this->call(TownsTableSeeder::class);
        $this->call(TransportationsTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(VisitsTableSeeder::class);
    }
}
