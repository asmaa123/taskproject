<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('types')->delete();
        
        \DB::table('types')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'الوثيقه التاني ',
                'center_id' => 2,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:33:38',
            ),
            1 => 
            array (
                'id' => 1,
                'name' => 'الوثيقه الاول ',
                'center_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-12-04 03:33:38',
            ),
        ));
        
        
    }
}